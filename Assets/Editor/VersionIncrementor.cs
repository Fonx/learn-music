﻿// Minor adjustments by Arshd and then tsny and finaly RKar
// Version Incrementor Script for Unity by Francesco Forno (Fornetto Games)
// Inspired by http://forum.unity3d.com/threads/automatic-version-increment-script.144917/

#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

[InitializeOnLoad]
class VersionIncrementor : IPreprocessBuildWithReport
{
    [MenuItem("Build/Increase Current Build Version")]
    private static void IncreaseBuild()
    {
        IncrementVersion(new int[] { 0, 0, 1 });
    }

    [MenuItem("Build/Increase Minor Version")]
    private static void IncreaseMinor()
    {
        IncrementVersion(new int[] { 0, 1, 0 });
    }

    [MenuItem("Build/Increase Major Version")]
    private static void IncreaseMajor()
    {
        IncrementVersion(new int[] { 1, 0, 0 });
    }

    static void IncrementVersion(int[] versionIncr)
    {
        string[] lines = PlayerSettings.bundleVersion.Split('.');

        for (int i = lines.Length - 1; i >= 0; i--)
        {
            bool isNumber = int.TryParse(lines[i], out int numberValue);

            if (isNumber && versionIncr.Length - 1 >= i)
            {
                if (i > 0 && versionIncr[i] + numberValue > 9)
                {
                    versionIncr[i - 1]++;

                    versionIncr[i] = 0;
                }
                else
                {
                    versionIncr[i] += numberValue;
                }
            }
        }

        PlayerSettings.bundleVersion = versionIncr[0] + "." + versionIncr[1] + "." + versionIncr[2];
    }

    public static string GetLocalVersion()
    {
        return PlayerSettings.bundleVersion.ToString();
    }

    public void OnPreprocessBuild(BuildReport report)
    {
        bool shouldIncrement = EditorUtility.DisplayDialog("Incrementer", "Applying increment? Current: v" + PlayerSettings.bundleVersion, "Yes", "No");

        if (shouldIncrement)
        {
            string[] numbers = PlayerSettings.bundleVersion.Split('.');
            string major = numbers[0];
            int minor = Convert.ToInt32(numbers[1]);
            minor++;
            PlayerSettings.bundleVersion = major + "." + minor;
        }
    }


    public int callbackOrder { get { return 0; } }
}
#endif