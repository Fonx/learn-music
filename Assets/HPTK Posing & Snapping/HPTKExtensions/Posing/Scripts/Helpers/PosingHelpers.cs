﻿using HPTK.Models.Avatar;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPTK.Helpers
{
    public static class PosingHelpers
    {
        public static void ApplyPose(HandModel hand, HPTK.Settings.HandPose pose, bool pos, bool rot, bool scale, bool inverted)
        {
            // Wrist won't be applied
            for (int f = 0; f < Mathf.Min(hand.fingers.Length, pose.fingers.Length); f++)
            {
                for (int b = 0; b < Mathf.Min(hand.fingers[f].bones.Length, pose.fingers[f].bones.Length); b++)
                {
                    if (pose.fingers[f].bones[b].space == Space.World)
                        Debug.LogError("poseBones[" + f + "] is not configured en local space!");

                    if (pos) hand.fingers[f].bones[b].transformRef.localPosition = inverted ? pose.fingers[f].bones[b].position * -1.0f : pose.fingers[f].bones[b].position;
                    if (rot) hand.fingers[f].bones[b].transformRef.localRotation = pose.fingers[f].bones[b].rotation;
                    if (scale) hand.fingers[f].bones[b].transformRef.localScale = pose.fingers[f].bones[b].localScale;
                }
            }
        }

        public static void MatchMasterWrist(MasterHandModel master, HandModel ghost)
        {
            // Wrist
            Transform ghostWristDestination = master.wrist.transformRef;

            if (master.wrist is MasterBoneModel && (master.wrist as MasterBoneModel).offset != null)
                ghostWristDestination = (master.wrist as MasterBoneModel).offset;

            ghost.wrist.transformRef.position = ghostWristDestination.position;
            ghost.wrist.transformRef.rotation = ghostWristDestination.rotation;

            // Forearm
            if (master.forearm && ghost.forearm)
            {
                ghost.forearm.transformRef.position = master.forearm.transformRef.position;
                ghost.forearm.transformRef.rotation = master.forearm.transformRef.rotation;
            }
        }

        public static void MatchMasterFingers(MasterHandModel master, HandModel ghost)
        {
            // Fingers
            for (int f = 0; f < Mathf.Min(master.fingers.Length, ghost.fingers.Length); f++)
            {
                for (int b = 0; b < Mathf.Min(master.fingers[f].bones.Length, ghost.fingers[f].bones.Length); b++)
                {
                    ghost.fingers[f].bones[b].transformRef.position = master.fingers[f].bones[b].transformRef.position;
                    ghost.fingers[f].bones[b].transformRef.rotation = master.fingers[f].bones[b].transformRef.rotation;
                }
            }
        }
    }
}
