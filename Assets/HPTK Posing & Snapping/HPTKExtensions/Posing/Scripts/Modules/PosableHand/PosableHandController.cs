﻿using HPTK.Helpers;
using HPTK.Models.Avatar;
using HPTK.Views.Handlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HPTK.Settings;
using HPTK.Models.Interaction;

namespace HPTK.Controllers.Avatar
{
    public class PosableHandController : PosableHandHandler
    {
        public PosableHandModel model;

        bool isActive = false;
        bool isSnapped = false;

        HandModel ghost;
        MasterHandModel master;
        SlaveHandModel slave;

        SnapperHandler snapper;

        bool validStart = true;

        private void Awake()
        {
            model.handler = this;
            viewModel = new ProxyHandViewModel(model);

            if (!AllSystemsNominal())
            {
                validStart = false;
                return;
            }

            // Master and ghost are mandatory
            master = model.proxyHand.master as MasterHandModel;
            ghost = model.proxyHand.ghost;

            // Slave hand is needed only for auto-snapping
            if (model.proxyHand.slave)
                slave = model.proxyHand.slave as SlaveHandModel;
        }

        private void Start()
        {
            // If we are using auto-snapping, ghost should only be visible as predicted pose
            if (slave)
            {
                ghost.skinnedMR.enabled = false;
            }
        }

        private void Update()
        {
            if (!validStart)
                return;

            if (slave)
            {
                UpdatePosePoint();

                // Get snapper on Update as Handlers are being self-registred in ProxyHandModel.relatedHandlers in their Start() functions
                if (!snapper)
                {
                    snapper = GetSnapper(model.proxyHand);

                    if (snapper)
                    {
                        snapper.onGraspSnap.AddListener(OnGraspSnap);
                        snapper.onUnsnap.AddListener(OnUnsnap);
                    }
                }
            }

            if (model.posePoint)
            {
                PosingHelpers.MatchMasterWrist(master, ghost);
                PosingHelpers.MatchMasterFingers(master, ghost);

                if (!model.ghostMatchesMastersWrist)
                {
                    PoseGeneration.HandPlacement(ghost, master, model.posePoint, model.configuration);
                }

                if (model.posePoint.viewModel.collideWithOtherRbs)
                    PoseGeneration.HandPose(ghost, model.startPose, model.endPose, model.posePoint, model.configuration, null);
                else
                    PoseGeneration.HandPose(ghost, model.startPose, model.endPose, model.posePoint, model.configuration, model.posePoint.viewModel.rigidbodyRef);
            }
            else
            {
                PosingHelpers.MatchMasterWrist(master, ghost);

                switch (model.whenPosePointIsNull)
                {
                    case DefaultPosingBehaviour.MatchMaster:
                        PosingHelpers.MatchMasterFingers(master, ghost);
                        break;
                    case DefaultPosingBehaviour.SetStartPose:
                        PosingHelpers.ApplyPose(ghost,model.startPose, false, true, false, false);
                        break;
                    case DefaultPosingBehaviour.SetEndPose:
                        PosingHelpers.ApplyPose(ghost, model.endPose, false, true, false, false);
                        break;
                    case DefaultPosingBehaviour.KeepLastPose:
                        // Nothing
                        break;
                }
            }
        }

        SnapperHandler GetSnapper(ProxyHandModel proxyHand)
        {
            for (int i = 0; i < proxyHand.relatedHandlers.Count; i++)
            {
                if (proxyHand.relatedHandlers[i] is SnapperHandler)
                    return proxyHand.relatedHandlers[i] as SnapperHandler;
            }

            return null;
        }


        void SetPosePoint(PosePointHandler posePoint)
        {
            model.posePoint = posePoint;

            if (!posePoint && isActive)
            {
                isActive = false;
                ghost.skinnedMR.enabled = false;
            }
            else if (posePoint && !isActive)
            {
                isActive = true;
                ghost.skinnedMR.enabled = true;
            }
        }

        void OnGraspSnap(InteractionModel interaction)
        {
            SetSlaveLimits(slave, ghost, model.posePoint);
            isSnapped = true;

            ghost.skinnedMR.enabled = false;
        }

        void OnUnsnap()
        {
            ResetSlaveLimits(slave);
            isSnapped = false;
        }

        void UpdatePosePoint()
        {
            if (master.graspLerp >= core.model.configuration.minLerpToGrasp / 2.0f && snapper.viewModel.graspableCandidate != null)
            {
                PosePointHandler posePoint = GetPosePoint(snapper.viewModel.graspableCandidate);

                if (posePoint)
                {
                    SetPosePoint(posePoint);
                }
            }
            else if (model.posePoint)
            {
                SetPosePoint(null);
            }
        }


        void SetFingerLimits(FingerModel slaveFinger, FingerModel ghostFinger)
        {
            if (slaveFinger.bones.Length != ghostFinger.bones.Length)
            {
                Debug.LogError(slaveFinger.name + "(" + slaveFinger.bones.Length + ") and " + ghostFinger.name + "(" + ghostFinger.bones.Length + ") don't have the same number of bones!");
                return;
            }

            SlaveBoneModel bone;

            for (int i = 0; i < ghostFinger.bones.Length; i++)
            {
                bone = slaveFinger.bones[i] as SlaveBoneModel;
                bone.minLocalRot = ghostFinger.bones[i].transformRef.localRotation;
            }
        }

        void SetSlaveLimits(SlaveHandModel slave, HandModel ghost, PosePointHandler posePoint)
        {
            if (posePoint.viewModel.limitThumb && slave.thumb && ghost.thumb)
                SetFingerLimits(slave.thumb, ghost.thumb);
            if (posePoint.viewModel.limitIndex && slave.index && ghost.index)
                SetFingerLimits(slave.index, ghost.index);
            if (posePoint.viewModel.limitMiddle && slave.middle && ghost.middle)
                SetFingerLimits(slave.middle, ghost.middle);
            if (posePoint.viewModel.limitRing && slave.ring && ghost.ring)
                SetFingerLimits(slave.ring, ghost.ring);
            if (posePoint.viewModel.limitPinky && slave.pinky && ghost.pinky)
                SetFingerLimits(slave.pinky, ghost.pinky);
        }

        void ResetSlaveLimits(SlaveHandModel slave)
        {
            SlaveBoneModel bone;
            for (int i = 0; i < slave.bones.Length; i++)
            {
                bone = slave.bones[i] as SlaveBoneModel;
                bone.minLocalRot = Quaternion.identity;
            }
        }

        PosePointHandler GetPosePoint(SnapPointHandler snapPoint)
        {
            for (int i = 0; i < snapPoint.viewModel.relatedHandlers.Length; i++)
            {
                if (snapPoint.viewModel.relatedHandlers[i] is PosePointHandler)
                    return snapPoint.viewModel.relatedHandlers[i] as PosePointHandler;
            }

            return null;
        }

        bool AllSystemsNominal()
        {
            if (!model.proxyHand.ghost)
            {
                Debug.LogError("ProxyHandModel doesn't have a HandModel available as ghost!");
                return false;
            }

            if (!model.proxyHand.master)
            {
                Debug.LogError("ProxyHandModel doesn't have a HandModel available as master!");
                return false;
            }

            return true;
        }
    }
}
