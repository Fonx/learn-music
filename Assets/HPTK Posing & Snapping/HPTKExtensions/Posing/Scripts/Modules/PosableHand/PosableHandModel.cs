﻿using HPTK.Models.Interaction;
using HPTK.Views.Handlers;
using HPTK.Models.Avatar;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HPTK.Settings;

namespace HPTK.Models.Avatar
{
    public enum DefaultPosingBehaviour
    {
        SetStartPose,
        SetEndPose,
        MatchMaster,
        KeepLastPose
    }

    public class PosableHandModel : HPTKModel
    {
        [HideInInspector]
        public PosableHandHandler handler;

        [Header("Handlers")]
        public PosePointHandler posePoint;

        [Header("Models")]
        public ProxyHandModel proxyHand;

        [Header("Control")]
        public bool ghostMatchesMastersWrist = false;
        public DefaultPosingBehaviour whenPosePointIsNull = DefaultPosingBehaviour.SetStartPose;
        public PosingConfiguration configuration;
        public Settings.HandPose startPose;
        public Settings.HandPose endPose;
    }
}