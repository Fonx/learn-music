﻿using HPTK.Controllers.Avatar;
using HPTK.Models.Avatar;
using HPTK.Views.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HPTK.Views.Handlers
{
    public class PosableHandHandler : HPTKHandler
    {
        public sealed class ProxyHandViewModel
        {
            PosableHandModel model;

            public PosePointHandler posePoint {
                get { return model.posePoint; }
                set {
                    model.posePoint = value;
                    model.handler.onSetPosePoint.Invoke(value);
                }
            }

            public ProxyHandViewModel(PosableHandModel model)
            {
                this.model = model;
            }
        }
        public ProxyHandViewModel viewModel;

        public PosePointEvent onSetPosePoint = new PosePointEvent();
    }
}
