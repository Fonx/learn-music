﻿using HPTK.Views.Handlers;
using HPTK;
using HPTK.Models.Interaction;
using HPTK.Views.Handlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPTK.Controllers.Interaction
{
    public class PosePointController : PosePointHandler
    {
        public PosePointModel model;

        private void Awake()
        {
            model.handler = this;
            viewModel = new PosePointViewModel(model);
        }

        private void Start()
        {
            if (model.snapPoint)
                model.snapPoint.relatedHandlers.Add(this);
        }
    }
}
