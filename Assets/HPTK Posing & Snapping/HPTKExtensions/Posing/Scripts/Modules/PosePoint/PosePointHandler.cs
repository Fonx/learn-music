﻿using HPTK.Helpers;
using HPTK.Views.Events;
using HPTK.Models.Interaction;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HPTK.Views.Handlers
{
    public class PosePointHandler : HPTKHandler
    {
        public sealed class PosePointViewModel
        {
            PosePointModel model;
            public Transform transformRef { get { return model.transformRef; } }
            public Rigidbody rigidbodyRef { get { return model.rigidbodyRef; } }
            public PosePointRotationMode rotationMode { get { return model.rotationMode; } }
            public PosePointPositionMode positionMode { get { return model.positionMode; } }
            public float minDistance { get { return model.minDistance; } }
            public float startAtLerp { get { return model.startAtLerp; } }
            public float stopAtLerp { get { return model.stopAtLerp; } }
            public float accuracy { get { return model.boneThickness; } }
            public bool collideWithOtherRbs { get { return model.collideWithOtherRbs; } }
            public bool collideWithTriggers { get { return model.collideWithTriggers; } }
            public bool limitThumb { get { return model.limitThumb; } }
            public bool limitIndex { get { return model.limitIndex; } }
            public bool limitMiddle { get { return model.limitMiddle; } }
            public bool limitRing { get { return model.limitRing; } }
            public bool limitPinky { get { return model.limitPinky; } }
            public PosePointViewModel(PosePointModel model)
            {
                this.model = model;
            }
        }
        public PosePointViewModel viewModel;

        // public UnityEvent onEvent = new UnityEvent();
    }
}
