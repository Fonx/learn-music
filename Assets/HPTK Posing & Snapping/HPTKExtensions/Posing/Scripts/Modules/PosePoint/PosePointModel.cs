﻿using HPTK.Helpers;
using HPTK.Views.Handlers;
using HPTK.Models.Interaction;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPTK.Models.Interaction
{
    public class PosePointModel : HPTKModel
    {
        [HideInInspector]
        public PosePointHandler handler;

        public SnapPointModel snapPoint;

        [Header("Refs")]
        public Transform transformRef;
        public Rigidbody rigidbodyRef;

        [Header("Control")]
        public PosePointRotationMode rotationMode = PosePointRotationMode.None;
        public PosePointPositionMode positionMode = PosePointPositionMode.Default;
        [Range(0.0f, 0.5f)]
        public float minDistance;

        [Range(0.0f, 1.0f)]
        public float startAtLerp = 0.0f;
        [Range(0.0f, 1.0f)]
        public float stopAtLerp = 1.0f;
        [Range(0.0f, 0.01f)]
        public float boneThickness = 0.008f;
        public bool collideWithOtherRbs = false;
        public bool collideWithTriggers = false;

        [Header("Finger specific")]
        public bool limitThumb = true;
        public bool limitIndex = true;
        public bool limitMiddle = true;
        public bool limitRing = true;
        public bool limitPinky = true;
    }
}
