﻿using HPTK.Views.Handlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HPTK.Views.Events
{
    public class PosePointEvent : UnityEvent<PosePointHandler> { }
}
