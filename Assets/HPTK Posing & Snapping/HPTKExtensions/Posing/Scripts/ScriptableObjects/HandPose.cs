﻿using HPTK.Input;
using HPTK.Models.Avatar;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HPTK.Settings;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace HPTK.Settings
{
    [CreateAssetMenu(menuName = "HPTK Posing/HandPose Asset", order = 2)]
    public class HandPose : ScriptableObject
    {
        public string alias;
        public Input.FingerPose[] fingers;
        public AbstractTsf wrist;
        public AbstractTsf forearm;

        public AbstractTsf[] GetBones()
        {
            List<AbstractTsf> bones = new List<AbstractTsf>();

            bones.Add(wrist);
            bones.Add(forearm);

            for (int i = 0; i < fingers.Length; i++)
            {
                bones.AddRange(fingers[i].bones);
            }

            return bones.ToArray();
        }

        /*
        public void Overwrite()
        {
            alias = pose.alias;
            List<Input.FingerPose> fingersList = new List<Input.FingerPose>();
            for (int i = 0; i < pose.fingers.Length; i++)
            {
                fingersList.Add(ToFingerPose(pose.fingers[i]));
            }
            fingers = fingersList.ToArray();
            wrist = ToAbstractTsf(pose.wrist);
            forearm = ToAbstractTsf(pose.forearm);
        }
        Input.FingerPose ToFingerPose(HandPoseGen.ScriptableObjects.FingerPose fingerPose)
        {
            Input.FingerPose newFinger = new Input.FingerPose(fingerPose.name);
            List<AbstractTsf> bones = new List<AbstractTsf>();
            for (int i = 0; i < fingerPose.bones.Length; i++)
            {
                bones.Add(ToAbstractTsf(fingerPose.bones[i]));
            }
            newFinger.bones = bones.ToArray();
            newFinger.tip = new AbstractTsf(fingerPose.name+"Tip", Space.Self);
            return newFinger;
        }
        AbstractTsf ToAbstractTsf(BonePose bonePose)
        {
            return new AbstractTsf(bonePose.localPosition,bonePose.localRotation,Space.Self,bonePose.localScale,bonePose.name);
        }
        */
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(HPTK.Settings.HandPose))]
public class customButton : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        HPTK.Settings.HandPose myScript = (HPTK.Settings.HandPose)target;

        /*
        if (GUILayout.Button("Overwrite"))
        {
            myScript.Overwrite();
        }
        */
    }

}
#endif