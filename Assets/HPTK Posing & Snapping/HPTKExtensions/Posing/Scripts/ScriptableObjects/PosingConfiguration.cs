﻿using HPTK.Helpers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPTK.Settings
{
    [CreateAssetMenu(menuName = "HPTK Posing/PosingConfiguration", order = 2)]
    public class PosingConfiguration : ScriptableObject
    {
        [Header("Control")]
        public float maxDistance = 0.5f;

        [Header("Performance")]
        [Range(10, 100)]
        public int maxIterations = 60;
        public bool onlyFingerTips = false;    
    }
}
