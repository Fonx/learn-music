﻿using HPTK.Models.Interaction;
using HPTK.Views.Handlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPTK.Helpers
{
    public static class SnappingHelpers
    {
        public static SnapPointHandler GetClosestSnapPoint(InteractionModel[] interactions, Vector3 handPoint, GestureType gesture, out InteractionModel interaction)
        {
            float d;
            float minDistance = Mathf.Infinity;

            SnapPointHandler[] snapPoints;

            interaction = null;
            SnapPointHandler bestCandidate = null;
            for (int i = 0; i < interactions.Length; i++)
            {
                snapPoints = GetSnapPointHandlers(interactions[i].interactable);

                for (int j = 0; j < snapPoints.Length; j++)
                {
                    switch (gesture)
                    {
                        case GestureType.Grasp:
                            if (!snapPoints[j].viewModel.isGraspable)
                                continue;
                            break;
                        case GestureType.Pinch:
                            if (!snapPoints[j].viewModel.isPinchable)
                                continue;
                            break;
                    }

                    d = Vector3.Distance(snapPoints[j].viewModel.transformRef.position, handPoint);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        interaction = interactions[i];
                        bestCandidate = snapPoints[j];
                    }
                }
            }

            return bestCandidate;
        }

        public static SnapPointHandler[] GetSnapPointHandlers(InteractableHandler interactable)
        {
            List<SnapPointHandler> handlers = new List<SnapPointHandler>();

            for (int i = 0; i < interactable.viewModel.relatedHandlers.Length; i++)
            {
                if (interactable.viewModel.relatedHandlers[i] is SnapPointHandler)
                {
                    handlers.Add(interactable.viewModel.relatedHandlers[i] as SnapPointHandler);
                }
            }

            return handlers.ToArray();
        }
    }
}
