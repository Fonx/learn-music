﻿using HPTK.Helpers;
using HPTK.Models.Interaction;
using HPTK.Views.Handlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPTK.Controllers.Interaction
{
    public class SnapPointController : SnapPointHandler
    {
        public SnapPointModel model;

        private void Awake()
        {
            model.handler = this;
            viewModel = new SnapPointViewModel(model);
        }

        private void Start()
        {
            onPinchSnap.AddListener(OnSnap);
            onGraspSnap.AddListener(OnSnap);
            onUnsnap.AddListener(OnUnsnap);
        }

        void OnSnap(InteractionModel interaction)
        {
            if (model.snapper)
                model.snapper.onUnsnap.Invoke();

            HPTKHandler[] relatedHandlers = interaction.interactor.viewModel.proxyHand.viewModel.relatedHandlers;
            for (int i = 0; i < relatedHandlers.Length; i++)
            {
                if (relatedHandlers[i] is SnapperHandler)
                {
                    model.snapper = relatedHandlers[i] as SnapperHandler;
                    break;
                }
            }
        }

        void OnUnsnap()
        {
            model.snapper = null;
        }
    }
}
