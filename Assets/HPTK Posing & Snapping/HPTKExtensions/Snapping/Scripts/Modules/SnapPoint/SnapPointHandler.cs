﻿using HPTK.Helpers;
using HPTK.Models.Interaction;
using HPTK.Views.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HPTK.Views.Handlers
{
    public class SnapPointHandler : HPTKHandler
    {
        public sealed class SnapPointViewModel
        {
            SnapPointModel model;
            public Transform transformRef { get { return model.transformRef; } }
            public Rigidbody rigidbodyRef { get { return model.interactable.rigidbodyRef; } }
            public bool isGraspable { get { return model.isGraspable; } }
            public bool isPinchable { get { return model.isPinchable; } }
            public bool ignoreGravity { get { return model.ignoreGravity; } }
            public bool ignoreHandCollisions { get { return model.ignoreHandCollisions; } }
            public HPTKHandler[] relatedHandlers { get { return model.relatedHandlers.ToArray(); } }
            public SnapPointViewModel(SnapPointModel model)
            {
                this.model = model;
            }
        }
        public SnapPointViewModel viewModel;

        public InteractionEvent onPinchSnap = new InteractionEvent();
        public InteractionEvent onGraspSnap = new InteractionEvent();
        public UnityEvent onUnsnap = new UnityEvent();
    }
}
