﻿using HPTK.Helpers;
using HPTK.Views.Handlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPTK.Models.Interaction
{
    public class SnapPointModel : HPTKModel
    {
        [HideInInspector]
        public SnapPointHandler handler;

        [HideInInspector]
        public SnapperHandler snapper;

        public InteractableModel interactable;

        public Transform transformRef;

        public bool isPinchable = true;
        public bool isGraspable = true;

        public bool ignoreGravity = false;

        public bool ignoreHandCollisions = false;

        [Header("Module registry")]
        public List<HPTKHandler> relatedHandlers = new List<HPTKHandler>();

        private void Start()
        {
            interactable.relatedHandlers.Add(handler);
        }
    }
}
