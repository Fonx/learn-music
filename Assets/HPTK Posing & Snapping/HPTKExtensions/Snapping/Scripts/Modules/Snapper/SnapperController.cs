﻿using HPTK.Helpers;
using HPTK.Models.Interaction;
using HPTK.Models.Avatar;
using HPTK.Views.Handlers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPTK.Controllers.Interaction
{
    public class SnapperController : SnapperHandler
    {
        public SnapperModel model;

        bool snapOnNextFrame = false;

        InteractionModel pinchableInteractionCandidate;
        InteractionModel graspableInteractionCandidate;

        bool wasUsingGravity = false;

        bool usingPose = false;

        SlaveHandModel slaveHand;
        ProxyHandHandler proxyHandHandler;
        InteractorHandler interactor;

        private void Awake()
        {
            model.handler = this;
            viewModel = new SnapperViewModel(model);
        }

        private void Start()
        {
            proxyHandHandler = model.proxyHand.handler;

            proxyHandHandler.onIndexPinch.AddListener(OnPinch);
            proxyHandHandler.onIndexUnpinch.AddListener(OnUnpinch);

            proxyHandHandler.onGrasp.AddListener(OnGrasp);
            proxyHandHandler.onUngrasp.AddListener(OnUngrasp);

            onUnsnap.AddListener(OnUnsnap);

            slaveHand = model.proxyHand.slave;
        }

        private void Update()
        {
            if (!interactor)
                interactor = GetInteractor(model.proxyHand);
            else
                UpdateIndicators();

            if (snapOnNextFrame)
            {
                snapOnNextFrame = false;
                SnapIfNeeded(proxyHandHandler, model);
            }
        }

        void OnUnpinch()
        {
            SnapIfNeeded(proxyHandHandler, model);           
        }

        void OnUngrasp()
        {
            SnapIfNeeded(proxyHandHandler, model);
        }

        void OnPinch()
        {
            SnapIfNeeded(proxyHandHandler, model);
        }

        void OnGrasp()
        {
            SnapIfNeeded(proxyHandHandler, model);
        }

        void SnapIfNeeded(ProxyHandHandler proxyHandHandler, SnapperModel snapper)
        {
            GestureType previousGesture = snapper.currentSnapGesture;

            // Si estabas pinchando y dejas de pinchar
            if (previousGesture == GestureType.Pinch && !proxyHandHandler.viewModel.isIndexPinching)
            {
                onUnsnap.Invoke();
                snapOnNextFrame = true;
                return;
            }

            // Si estabas agarrando y dejas de agarrar
            if (previousGesture == GestureType.Grasp && !proxyHandHandler.viewModel.isGrasping)
            {
                onUnsnap.Invoke();
                snapOnNextFrame = true;
                return;
            }

            GestureType newGesture = GestureType.None;
            Vector3 closestToThis = Vector3.positiveInfinity;
            SnapPointHandler newSnapPoint;

            // Si ahora pincheas
            if (previousGesture != GestureType.Pinch && proxyHandHandler.viewModel.isIndexPinching)
            {
                if (previousGesture == GestureType.Grasp && model.pinchableCandidate)
                {
                    onUnsnap.Invoke();
                    snapOnNextFrame = true;
                    return;
                }

                Debug.Log("Ahora pincheas...");
                newGesture = GestureType.Pinch;
                closestToThis = snapper.proxyHand.slave.pinchCenter.position;
            }

            // Si no hacias nada (ni pinchear) y ahora graspeas
            else if (previousGesture == GestureType.None && proxyHandHandler.viewModel.isGrasping)
            {
                Debug.Log("Ahora graspeas...");
                newGesture = GestureType.Grasp;
                closestToThis = slaveHand.palmCenter.position;
            }

            if (newGesture != GestureType.None && closestToThis != Vector3.positiveInfinity)
            {
                InteractionModel interaction;

                switch (newGesture)
                {
                    case GestureType.Grasp:
                        newSnapPoint = model.graspableCandidate;
                        interaction = graspableInteractionCandidate;
                        break;
                    case GestureType.Pinch:
                        newSnapPoint = model.pinchableCandidate;
                        interaction = graspableInteractionCandidate;
                        break;
                    default:
                        newSnapPoint = null;
                        interaction = null;
                        break;
                }

                if (newSnapPoint)
                {
                    model.currentSnapPoint = newSnapPoint;
                    model.currentSnapGesture = newGesture;

                    Rigidbody body = interaction.interactable.viewModel.rigidbodyRef;
                    Transform snapPoint = model.currentSnapPoint.viewModel.transformRef;
                    Rigidbody connectedBody =  null;

                    Vector3 destinationPosition = Vector3.zero;
                    Quaternion destinationRotation = Quaternion.identity;

                    if (newGesture == GestureType.Grasp)
                    {
                        model.currentSnapPoint.onGraspSnap.Invoke(interaction);

                        Debug.Log("Grasp!");
                        connectedBody = (slaveHand.wrist as SlaveBoneModel).rigidbodyRef;

                        destinationPosition = slaveHand.palmCenter.position;
                        destinationRotation = slaveHand.palmCenter.rotation;
                    }
                    else if (newGesture == GestureType.Pinch)
                    {
                        model.currentSnapPoint.onPinchSnap.Invoke(interaction);

                        Debug.Log("Pinch!");
                        connectedBody = (slaveHand.index.distal as SlaveBoneModel).rigidbodyRef;

                        destinationPosition = slaveHand.pinchCenter.position;
                        destinationRotation = slaveHand.pinchCenter.rotation;
                    }
                    else
                        Debug.LogError("Unsupported gesture detected!");

                    Vector3 pointPosition = Vector3.zero;
                    Quaternion pointRotation = Quaternion.identity;
                    
                    pointPosition = model.currentSnapPoint.viewModel.transformRef.position;
                    pointRotation = model.currentSnapPoint.viewModel.transformRef.rotation;

                    if (newGesture == GestureType.Grasp && GetPosePoint(model.currentSnapPoint) != null)
                    {
                        pointPosition = model.proxyHand.ghost.palmCenter.position;
                        pointRotation = model.proxyHand.ghost.palmCenter.rotation;

                        usingPose = true;
                    }

                    if (model.currentSnapPoint.viewModel.ignoreGravity)
                    {
                        wasUsingGravity = body.useGravity;
                        body.useGravity = false;
                    }
             
                    PhysHelpers.IgnoreBoneCollisions(body,slaveHand,true);

                    if (usingPose && !model.currentSnapPoint.viewModel.ignoreHandCollisions)
                    {
                        StartCoroutine(PhysHelpers.DoAfter(model.configuration.secondsToCompleteSnap, () =>
                        {
                            PhysHelpers.IgnoreBoneCollisions(body, slaveHand, false);
                        }));
                    }

                    model.currentSnapJoint = PhysHelpers.CreateSnapJoint(
                        body,
                        connectedBody,
                        pointPosition,
                        pointRotation,
                        destinationPosition,
                        destinationRotation,
                        true
                        );

                    StartCoroutine(PhysHelpers.SmoothLerpJointSpring(
                        model.currentSnapJoint,
                        model.currentSnapJoint.linearLimitSpring,
                        model.configuration.snapLimitSpring.toSoftJointLimitSpring(),
                        model.currentSnapJoint.slerpDrive,
                        model.configuration.snapSlerpDrive.toJointDrive(),
                        model.configuration.secondsToCompleteSnap)
                        );

                    if (newGesture == GestureType.Grasp)
                    {
                        onGraspSnap.Invoke(interaction);
                    }
                    else if (newGesture == GestureType.Pinch)
                    {
                        onPinchSnap.Invoke(interaction);
                    }

                }
                else
                {
                    Debug.Log("Any snappoint was found.");
                }
            }
        }

        void OnUnsnap()
        {
            Rigidbody rb = model.currentSnapJoint.GetComponent<Rigidbody>();

            if (model.currentSnapPoint.viewModel.ignoreGravity)
                rb.useGravity = wasUsingGravity;

            PhysHelpers.FreeJoint(model.currentSnapJoint);
            Destroy(model.currentSnapJoint);

            if (usingPose)
            {
                PhysHelpers.IgnoreBoneCollisions(rb, slaveHand, true);
                usingPose = false;
            }

            StartCoroutine(PhysHelpers.DoAfter(model.configuration.secondsToCompleteSnap, () =>
            {
                PhysHelpers.IgnoreBoneCollisions(rb, slaveHand, false);
            }));

            model.currentSnapPoint.onUnsnap.Invoke();

            model.currentSnapPoint = null;
            model.currentSnapGesture = GestureType.None;
        }

        void UpdateIndicators()
        {
            model.pinchableCandidate = SnappingHelpers.GetClosestSnapPoint(
                    interactor.viewModel.interactions,
                    slaveHand.pinchCenter.position,
                    GestureType.Pinch,
                    out pinchableInteractionCandidate
                );

            model.graspableCandidate = SnappingHelpers.GetClosestSnapPoint(
                    interactor.viewModel.interactions,
                    slaveHand.palmCenter.position,
                    GestureType.Grasp,
                    out graspableInteractionCandidate
                );

            if (model.pinchableCandidate)
            {
                model.pinchableIndicator.position = model.pinchableCandidate.viewModel.transformRef.position;
                if (!model.pinchableIndicator.gameObject.activeSelf)
                    model.pinchableIndicator.gameObject.SetActive(true);
            }
            else
            {
                if (model.pinchableIndicator.gameObject.activeSelf)
                    model.pinchableIndicator.gameObject.SetActive(false);
            }

            if (model.graspableCandidate)
            {
                model.graspableIndicator.position = model.graspableCandidate.viewModel.transformRef.position;
                if (!model.graspableIndicator.gameObject.activeSelf)
                    model.graspableIndicator.gameObject.SetActive(true);
            }
            else
            {
                if (model.graspableIndicator.gameObject.activeSelf)
                    model.graspableIndicator.gameObject.SetActive(false);
            }
        }

        InteractorHandler GetInteractor(ProxyHandModel proxyHand)
        {
            for (int i = 0; i < proxyHand.relatedHandlers.Count; i++)
            {
                if (proxyHand.relatedHandlers[i] is InteractorHandler)
                {
                    return proxyHand.relatedHandlers[i] as InteractorHandler;
                }
            }

            return null;
        }

        PosePointHandler GetPosePoint(SnapPointHandler snapPoint)
        {
            for (int i = 0; i < snapPoint.viewModel.relatedHandlers.Length; i++)
            {
                if (snapPoint.viewModel.relatedHandlers[i] is PosePointHandler)
                {
                    return snapPoint.viewModel.relatedHandlers[i] as PosePointHandler;
                }
            }

            return null;
        }

    }
}
