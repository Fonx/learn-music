﻿using HPTK.Helpers;
using HPTK.Models.Interaction;
using HPTK.Views.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace HPTK.Views.Handlers
{
    public class SnapperHandler : HPTKHandler
    {
        public sealed class SnapperViewModel
        {
            SnapperModel model;
            public SnapPointHandler snapPoint { get { return model.currentSnapPoint; } }
            public GestureType gesture { get { return model.currentSnapGesture; } }
            public ConfigurableJoint joint { get { return model.currentSnapJoint; } }
            public SnapPointHandler pinchableCandidate { get { return model.pinchableCandidate; } }
            public SnapPointHandler graspableCandidate { get { return model.graspableCandidate; } }
            public ProxyHandHandler proxyHand { get { return model.proxyHand.handler; } }

            public SnapperViewModel(SnapperModel model)
            {
                this.model = model;
            }
        }
        public SnapperViewModel viewModel;

        public InteractionEvent onPinchSnap = new InteractionEvent();
        public InteractionEvent onGraspSnap = new InteractionEvent();
        public UnityEvent onUnsnap = new UnityEvent();
    }
}
