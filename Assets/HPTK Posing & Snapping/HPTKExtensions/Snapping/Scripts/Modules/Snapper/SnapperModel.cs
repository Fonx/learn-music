﻿using HPTK.Helpers;
using HPTK.Models.Avatar;
using HPTK.Settings;
using HPTK.Views.Handlers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPTK.Models.Interaction
{
    [Serializable]
    public class SnapperModel : HPTKModel
    {
        [HideInInspector]
        public SnapperHandler handler;

        public SnappingConfiguration configuration;

        public ProxyHandModel proxyHand;

        public Transform pinchableIndicator;
        public Transform graspableIndicator;

        [Header("Updated by SnapperController")]
        public SnapPointHandler pinchableCandidate;
        public SnapPointHandler graspableCandidate;
        public SnapPointHandler currentSnapPoint;
        public ConfigurableJoint currentSnapJoint;
        public GestureType currentSnapGesture;

        private void Start()
        {
            proxyHand.relatedHandlers.Add(handler);
        }
    }
}
