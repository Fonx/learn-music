﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HPTK.Settings
{
    [CreateAssetMenu(menuName = "HPTK Snapping/SnappingConfiguration Asset", order = 2)]
    public class SnappingConfiguration : ScriptableObject
    {
        public CustomJointDrive snapLimitSpring;
        public CustomJointDrive snapSlerpDrive;
        public float secondsToCompleteSnap;
    }
}
