﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchMechanical : MonoBehaviour {

    public Sprite onIcon;   //quando true, o botão deve mostrar o icone de pause
    public Sprite offIcon;  //quando false, o botão de mostrar o icone de start
    private bool currentState; 

    // Use this for initialization
    private void Start () {

        currentState = false;
	}
	
    public void HoldState()
    {
        currentState = !currentState;
        if (currentState)
        {
            gameObject.transform.Find("Icon").GetComponent<Image>().sprite = onIcon;
            //gameObject.transform.Find("Icon Highlighted").GetComponent<Image>().sprite = onIcon;
            gameObject.transform.Find("Image").Find("Icon Highlighted").GetComponent<Image>().sprite = onIcon;
        }
        else
        {
            gameObject.transform.Find("Icon").GetComponent<Image>().sprite = offIcon;
            //gameObject.transform.Find("Icon Highlighted").GetComponent<Image>().sprite = offIcon;
            gameObject.transform.Find("Image").Find("Icon Highlighted").GetComponent<Image>().sprite = offIcon;
        }
    }
    

	// Update is called once per frame
	//private void Update () {
		
	//}
}
