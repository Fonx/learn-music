﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class BackgroundColorControl : MonoBehaviour
{
    public enum Mode
    {
        Image, Color, CameraBackground
    }
    public Mode mode = Mode.Color;
    public Image backgroundImage;
    public Text text;
    public List<Sprite> sprites;
    public List<string> textMenu;
    public List<Color> colors;
    int currentIndex;
    public static UnityAction<int> ColorChange;
    private Camera _camera;

    private void Awake() 
    {
        _camera = Camera.main;
    }
    void Start()
    {
        currentIndex=0;
    }

    // Update is called once per frame
    public void SwapBackground()
    {
        switch (mode)
        {
            case Mode.Color:
            ColorModeSwap();
            break;
            case Mode.Image:
            ImageModeSwap();
            break;
            case Mode.CameraBackground:
            CameraColorModeSwap();
            break;
            default:
            break;
        }
    }

    void ImageModeSwap()
    {
        currentIndex++;
        if(currentIndex>=textMenu.Count) currentIndex=0;
        backgroundImage.sprite=sprites[currentIndex];
        text.text=textMenu[currentIndex];
    }
    void ColorModeSwap()
    {
        currentIndex++;
        if(currentIndex>=colors.Count) currentIndex=0;
        backgroundImage.color=colors[currentIndex];
        text.text=textMenu[currentIndex];
        if(ColorChange!=null) ColorChange.Invoke(currentIndex);
    }
    void CameraColorModeSwap()
    {
        currentIndex++;
        if(currentIndex>=colors.Count) currentIndex=0;
        //backgroundImage.color=colors[currentIndex];
        _camera.backgroundColor = colors[currentIndex];
        text.text=textMenu[currentIndex];
        if(ColorChange!=null) ColorChange.Invoke(currentIndex);
    }
}
