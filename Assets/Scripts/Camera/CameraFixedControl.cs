﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFixedControl : MonoBehaviour
{
    public enum CameraType { ANALISE, FIXED }
    public CameraType cameraMode = CameraType.FIXED;
    // REFERENCIAS DA CAMERA

    [Header("REFERENCIAS DA CÂMERA FIXA")]
    [SerializeField] private Animator camAnimator;
    [SerializeField] private Transform cameraFixed;

    [Header("REFERENCIAS DA CÂMERA DE ANALISE")]
    [SerializeField] private CameraTranslateFollowPathCommand cameraTranslateFollowPathCommand;
    [SerializeField] private Transform cameraAnalizeTransform;

    // FIM DAS REFERENCIAS DA CAMERA
    public string[] positions; // Precisa ser o mesmo nome da boleana dentro do animator controller

    // METODOS 
    public void SetCameraPosition(int PositionIndex)
    {
        if (cameraMode == CameraType.ANALISE) SetFixedCam();
        if (positions[PositionIndex] == "Analize") { SetAnalizeCam(); }
    }

    private void SetAnalizeCam()
    {
        TurnAnalizeCamOnOff(true);
        cameraMode = CameraType.ANALISE;
    }

    private void SetFixedCam()
    {
        TurnAnalizeCamOnOff(false);
        cameraMode = CameraType.FIXED;

        cameraFixed.position = cameraAnalizeTransform.position;
        cameraFixed.rotation = cameraAnalizeTransform.rotation;
    }

    private void TurnAnalizeCamOnOff(bool onOff)
    {
        cameraTranslateFollowPathCommand.enabled = onOff;
        cameraAnalizeTransform.parent.parent.gameObject.SetActive(onOff);
    }

}
