﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraFocusControl : MonoBehaviour
{
    Camera _camera;
    void Awake()
    {
        _camera=this.GetComponent<Camera>();
    }

    private void OnEnable() 
    {
        CameraTargetControl.targetPositionChange += UpdateLookAt;
    }
    private void OnDisable() 
    {
        CameraTargetControl.targetPositionChange -= UpdateLookAt;
    }
    void UpdateLookAt(Vector3 target)
    {
        _camera.transform.LookAt(target);
    }
}
