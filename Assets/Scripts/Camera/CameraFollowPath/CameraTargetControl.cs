﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class CameraTargetControl : MonoBehaviour
{
    public float targetTranslateSpeed = 0;
    private Vector3 clickPosition;
    [SerializeField] private Camera myCamera;
    [SerializeField] private Camera RayCastCameraOrigin;
    private Vector3 originalTargetPosition;
    private bool canFocus;
    public static UnityAction<Vector3> targetPositionChange; //Sempre envia o position e não o local position (para manter a funcionalidade do script)

    private void Start()
    {
        originalTargetPosition = this.transform.position;
    }
    public void PrepareToFocus()
    {
        FocusOn();
        canFocus = true;
    }

    private void FocusOn()
    {
        StartCoroutine(GetMouseClickInputToFocus());
    }

    IEnumerator GetMouseClickInputToFocus()
    {
        bool positionSet = false;
        RaycastHit hitPosition = new RaycastHit();
        while (!positionSet)
        {

            if (Input.GetKey(KeyCode.Mouse0))
            {
                var ray = RayCastCameraOrigin.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hitPosition, 10, RayCastCameraOrigin.cullingMask))
                {
                    clickPosition = hitPosition.point;
                    positionSet = true;
                }
            }
            yield return null;
        }
        if (canFocus)
        {
            StartCoroutine(SetFocusPositionSmooth());
            canFocus = false;
        }
    }

    void SetFocusPosition()
    {
        //transform.position = clickPosition;
        SetPosition(clickPosition);
        //myCamera.transform.LookAt(clickPosition);
    }

    IEnumerator SetFocusPositionSmooth()
    {
        float increment = 0;
        while (transform.position != clickPosition)
        {
            increment += targetTranslateSpeed;
            //transform.position = Vector3.Slerp(transform.position, clickPosition, increment);
            SetPosition(Vector3.Slerp(transform.position, clickPosition, increment));
            //myCamera.transform.LookAt(transform.position);
            yield return null;
        }
    }

    public void SetTargetPosition(Vector3 value)
    {
        clickPosition = value;
        StartCoroutine(SetFocusPositionSmoothLocal());
    }
    IEnumerator SetFocusPositionSmoothLocal()
    {
        float increment = 0;
        while (transform.localPosition != clickPosition)
        {
            increment += targetTranslateSpeed;
            //transform.localPosition = Vector3.Slerp(transform.localPosition, clickPosition, increment);
            SetLocalPosition(Vector3.Slerp(transform.localPosition, clickPosition, increment));
            //myCamera.transform.LookAt(transform.position);
            yield return null;
        }
    }

    void SetLocalPosition(Vector3 newPos)
    {
        this.transform.localPosition = newPos;
        if (targetPositionChange != null) targetPositionChange.Invoke(transform.position);
    }

    void SetPosition(Vector3 newPos)
    {
        this.transform.position = newPos;
        if (targetPositionChange != null) targetPositionChange.Invoke(newPos);
    }
}
