﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraTranslateFollowPathCommand : MonoBehaviour
{
    public float ScreenOffsetToStartTranslate = 15;
    private CameraTraslateFollowPath myCamera;  //Movimento horizontal
    private CameraTranslateFollowPathVertical myCameraVertical; //Movimento Vertical
    private CameraZoomFollowPath myCameraZoom;   // Movimento de zoom
    private CPC_CameraPath cameraPath;
    private CameraTranslateVerticalPan cameraTranslateVerticalPan;
    private int dirHorizontal, dirVertical, dirZoom;

    private void Awake()
    {
        myCamera = GetComponentInChildren<CameraTraslateFollowPath>();
        myCameraVertical = GetComponent<CameraTranslateFollowPathVertical>();
        myCameraZoom = GetComponentInChildren<CameraZoomFollowPath>();
        cameraTranslateVerticalPan = GetComponentInChildren<CameraTranslateVerticalPan>();
        cameraPath = GetComponent<CPC_CameraPath>();
    }


    #region ANALISE MODE
    private void Update()
    {
        CallCameraTranslate();
        CallCameraVerticalTranslate();
        CallCameraZoom();
        if (Input.GetKeyDown(KeyCode.Mouse2))
        {
            cameraTranslateVerticalPan.StartPan();
        }
    }

    //HORIZONTALTRANSLATE
    void CallCameraTranslate() // Foca na translação horizontal da camera
    {
        float RightLimit = Screen.width - ScreenOffsetToStartTranslate;
        float LeftLimit = ScreenOffsetToStartTranslate;

        if (Input.mousePosition.x > RightLimit) // Verifica se o mouse esta no canto direito da tela para iniciar a translação
        {
            dirHorizontal = 1;                  // Configura direção para direita
            Translate();                        // Efetuar translado horizontal
        }
        if (Input.mousePosition.x < LeftLimit)  // Verifica se o mouse esta no canto esquerdo da tela para iniciar a translação
        {
            dirHorizontal = -1;                 // Configura direção para esquerda
            Translate();                        // Efetuar translado horizontal
        }
        stoptranslation();                      // Garante que a camera vai parar ao retirar o mouse das laterais da tela
    }
    void Translate()                            // Translado Horizontal
    {
        myCamera.TranslateWithMouse(dirHorizontal);
        myCameraZoom.AjustCameraZoomWhenTranslate();
    }
    void stoptranslation()                      // Para o translado horizontal
    {
        myCamera.StopTranslateWithMouse();
    }


    //VERTICALTRANSLATE
    void CallCameraVerticalTranslate()
    {
        float UpLimit = Screen.height - ScreenOffsetToStartTranslate;
        float DownLimit = ScreenOffsetToStartTranslate;

        if (Input.mousePosition.y > UpLimit)    // Verifica se o mouse esta na parte superior da tela para iniciar a translação
        {
            dirVertical = 1;                    // Configura direção para direita
            TranslateVertical();                // Efetuar translado Vertical
            myCameraZoom.AjustCameraZoomWhenTranslate();
        }
        if (Input.mousePosition.y < DownLimit)  // Verifica se o mouse esta na parte inferior da tela para iniciar a translação
        {
            dirVertical = -1;                   // Configura direção para esquerda
            TranslateVertical();                // Efetuar translado Vertical
            myCameraZoom.AjustCameraZoomWhenTranslate();
        }
    }

    void TranslateVertical()
    {
        float heightIncrement = 0.01f * dirVertical;
        myCameraVertical.SetCameraHeight(heightIncrement);
        cameraPath.PlayPathUpdateHeight();
    }

    //ZOOM TRANSLATE

    public void CallCameraZoom()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            dirZoom = 1;
            Zoom();
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            dirZoom = -1;
            Zoom();
        }

    }
    void Zoom()
    {
        myCameraZoom.Zoom(dirZoom);
    }
    #endregion

}