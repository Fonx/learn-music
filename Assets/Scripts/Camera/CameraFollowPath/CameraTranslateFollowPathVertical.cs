﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTranslateFollowPathVertical : MonoBehaviour
{
    public float upVerticalLimit, downVerticalLimit;
    private CPC_CameraPath cameraPath;
    private List<CPC_Point> waypoints;

    private void Awake()
    {
        cameraPath = GetComponent<CPC_CameraPath>();
    }

    private void Start()
    {
        waypoints = cameraPath.points;
    }

    public void SetCameraHeight(float newHeight)
    {
        UpdateCameraHeight(newHeight);
    }

    void UpdateCameraHeight(float height)
    {
        foreach (CPC_Point item in waypoints)
        {
            if (item.position.y + height > upVerticalLimit || item.position.y + height < downVerticalLimit) height = 0;
            item.position = new Vector3(item.position.x, item.position.y + height, item.position.z);
        }
        cameraPath.points = waypoints;
    }
}
