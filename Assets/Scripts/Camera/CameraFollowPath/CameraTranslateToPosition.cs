﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTranslateToPosition : MonoBehaviour
{
    public float translateSpeed;
    [SerializeField] private List<Transform> positions;
    [SerializeField] private Transform positionsParent;
    private CPC_CameraPath cameraSystem;
    private Vector3 positionsParentInitialPosition;


    private void Awake()
    {
        cameraSystem = GetComponent<CPC_CameraPath>();
        positionsParentInitialPosition = positionsParent.position;
    }
    private void Start()
    {
        translateCameraSystemToPosition(0);
    }

    public void translateCameraSystemToPosition(int index)
    {
        StopAllCoroutines();
        StartCoroutine(translateCameraSystemToPositionSmooth(index));
    }

    IEnumerator translateCameraSystemToPositionSmooth(int index)
    {

        float increment = 0;
        int i = 0;
        Vector3 initialTransformPosition = transform.position;
        Vector3 initialDestinyPosition = positions[index].position;
        Vector3 translateVector = new Vector3(positions[index].position.x - transform.position.x, 0, positions[index].position.z - transform.position.z);

        List<Vector3> pointsOrigin = new List<Vector3>();

        foreach (CPC_Point item in cameraSystem.points)
        {
            Vector3 vectorToAdd = item.position;
            pointsOrigin.Add(vectorToAdd);
        }

        while (increment <= 1)
        {
            increment += translateSpeed;
            if (increment >= 1) increment = 1;
            i = 0;
            foreach (CPC_Point item in cameraSystem.points)
            {
                item.position = pointsOrigin[i] + Vector3.Lerp(
                    Vector3.zero,
                    translateVector,
                    increment);
                i++;
            }
            transform.position = initialTransformPosition + Vector3.Lerp(
                    Vector3.zero,
                    translateVector,
                    increment);
            positionsParent.position = positionsParentInitialPosition;

            if (increment >= 1)
            {
                pointsOrigin.Clear();
                StopAllCoroutines();
            }
            yield return null;
        }

    }
}