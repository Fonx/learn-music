﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTranslateVerticalPan : MonoBehaviour
{
    public Transform targetTransform;
    Vector2 originClickPosition;
    public CameraTranslateFollowPathVertical myCameraVertical;
    public CameraTraslateFollowPath myCameraHorizontal;
    public CameraZoomFollowPath myCameraZoom;
    public CPC_CameraPath cameraPath;

    // VOCE ESTA FAZENDO O DESLOCAMENTO PANORAMICO DA CAMERA AO CLICAR BOTAO DO MEIO DO MOUSE

    public void StartPan()
    {
        GetMouseMidlleClickPosition();
    }
    public void GetMouseMidlleClickPosition()
    {
        originClickPosition = Input.mousePosition;
        if (!IsInvoking("CheckDirectionToVerticalPan"))
            InvokeRepeating("CheckDirectionToVerticalPan", 0, 0.01f);
        if (!IsInvoking("CheckDirectionToRotateWithMiddleMouse"))
            InvokeRepeating("CheckDirectionToRotateWithMiddleMouse", 0, 0.01f);
    }
    private void CheckDirectionToVerticalPan()
    {
        if (!Input.GetKey(KeyCode.Mouse2)) CancelInvoke("CheckDirectionToVerticalPan");
        float temp = Input.mousePosition.y - originClickPosition.y;
        float dir = Mathf.Sign(temp);
        if (Mathf.Abs(temp) > 20)
            VerticalPan(dir);
    }
    private void VerticalPan(float dir)
    {
        float translateSpeed = 0.005f;
        if (targetTransform.position.y - translateSpeed * -dir < 0) { dir = 0; targetTransform.position = new Vector3(targetTransform.position.x, 0, targetTransform.position.z); }
        if (targetTransform.position.y + translateSpeed * dir > 1.8f) { dir = 0; targetTransform.position = new Vector3(targetTransform.position.x, 1.8f, targetTransform.position.z); }
        targetTransform.position += new Vector3(0, translateSpeed * dir, 0);
        myCameraVertical.SetCameraHeight(translateSpeed * dir);
        cameraPath.PlayPathUpdateHeight();
    }
    private void CheckDirectionToRotateWithMiddleMouse()
    {
        if (!Input.GetKey(KeyCode.Mouse2)) CancelInvoke("CheckDirectionToRotateWithMiddleMouse");
        float temp = Input.mousePosition.x - originClickPosition.x;
        float dir = Mathf.Sign(temp);
        if (Mathf.Abs(temp) > 30)
            RotateWithMiddleMouse(dir);
    }
    private void RotateWithMiddleMouse(float dir)
    {
        myCameraHorizontal.TranslateWithMouse((int)dir);
        myCameraZoom.AjustCameraZoomWhenTranslate();
    }
}