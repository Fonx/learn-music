﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTraslateFollowPath : MonoBehaviour
{
    private CPC_CameraPath cameraPath;
    private float translateIncrement = 0;
    private int waypoint = 0;

    private void Awake()
    {
        cameraPath = GetComponentInParent<CPC_CameraPath>();
    }
    public void TranslateWithMouse(int direction)
    {
        translate(direction);
    }
    public void StopTranslateWithMouse()
    {
        cameraPath.StopPath();
    }

    void playPath()
    {
        cameraPath.StopPath();
        if (!cameraPath.IsPlaying())
        {
            cameraPath.PlayPath(5, waypoint, translateIncrement);
        }
        if (translateIncrement > .99f)
        {
            waypoint++;
            translateIncrement = 0;
            cameraPath.SetCurrentWayPoint(waypoint);
            ConstrainWaypoint();

        }
        if (translateIncrement < 0)
        {
            waypoint--;
            translateIncrement = .99f;
            cameraPath.SetCurrentWayPoint(waypoint);
            ConstrainWaypoint();
        }
    }

    void translate(int newDirection)
    {
        translateIncrement += 0.01f * newDirection;
        playPath();
        cameraPath.SetCurrentTimeInWaypoint(translateIncrement);
    }

    void ConstrainWaypoint()
    {
        if (waypoint >= cameraPath.points.Count - 1) { waypoint = 0; playPath(); }
        if (waypoint < 0) { waypoint = cameraPath.points.Count - 2; }
    }
}
