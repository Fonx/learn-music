﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomFollowPath : MonoBehaviour
{
    public float targetOffset;
    public float zoomSpeed;

    [SerializeField] private Transform target;
    [SerializeField] private Transform backLimit;

    private int zoomDirection;
    private float distanceToTarget;
    private float totalDistance;
    private float increment = 0;
    private bool called = false;
    private bool updateLimitOnce = true;

    private void Awake()
    {
        updateDistances();
    }
    public void Zoom(int direction)
    {
        MoveCameraZoom(direction);
    }

    private void MoveCameraZoom(int direction)
    {
        zoomDirection = direction;

        if (direction > 0)
        {
            if (Vector3.Distance(transform.position, target.position - transform.forward.normalized * targetOffset) > zoomSpeed * Time.deltaTime)
                transform.position += transform.forward * direction * zoomSpeed * Time.deltaTime * CheckIfCanZoom();
            else
                transform.position = target.position - transform.forward.normalized * targetOffset;
        }
        else
        {
            if (distanceToTarget < totalDistance)
                transform.position += transform.forward * direction * zoomSpeed * Time.deltaTime * CheckIfCanZoom();
            else
                transform.position = backLimit.position;
        }
        updateDistances();
    }

    private float CheckIfCanZoom() // Verifica se a camera esta entre o alvo e o limite de tras
    {
        if (zoomDirection == 1 && distanceToTarget <= targetOffset)
        {

            return 0;
        }
        if (zoomDirection == -1 && distanceToTarget >= totalDistance)
        {

            return 0;
        }
        return 1;
    }
    public void AjustCameraZoomWhenTranslate()
    {
        if (updateLimitOnce) { updateDistances(); updateLimitOnce = false; }
        if (!called)
        {
            StartCoroutine(ajustZoomSmoth());
        }
    }

    IEnumerator ajustZoomSmoth() // ajusta o zoom da camera ao transladar
    {
        called = true;
        increment = 0;
        while (transform.position != target.position - transform.forward.normalized * distanceToTarget) // Enquanto a camera não esta posicionada na mesma distancia em relaçã ao target
        {
            float sense = (distanceToTarget * 100) / (totalDistance - targetOffset); // define sensibilidade do ajuste baseado na distancia da camera para o target
            increment += 0.002f * sense;
            UpdateTotalDistance();

            if (distanceToTarget >= totalDistance)   // evita que a camera der um zoom negativo e se posicione atras do back limit
                transform.position = backLimit.position;
            else if (distanceToTarget <= targetOffset)
                transform.position = target.position - transform.forward.normalized * targetOffset;
            else
                transform.position = Vector3.Slerp(transform.position, target.position - transform.forward.normalized * distanceToTarget, increment); // ajusta a posição da camera considerando quão perto ela esta do alvo
            yield return null;
        }

        called = false;
    }

    void updateDistances()
    {
        distanceToTarget = (transform.position - target.position).magnitude;
        totalDistance = (target.position - backLimit.position).magnitude;
    }
    void UpdateTotalDistance()
    {
        totalDistance = (target.position - backLimit.position).magnitude;
    }
}