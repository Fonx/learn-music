﻿using System;
using UnityEngine;

namespace AcidezLaranja
{
    public class CameraDriver : MonoBehaviour//Receiver
    {
        [SerializeField] float translateStep = 0.01f;
        [SerializeField] float zoomStep = 0.02f;
        [SerializeField] float maximumDisplacementRadius = 0.5f;
        Vector3 displacement = Vector3.zero;
        Vector3 initialPosition = Vector3.zero;
        
        public void ZoomIn() => Translate(Direction.Forward);

        public void ZoomOut() => Translate(Direction.Back);

        public void TranslateUp() => Translate(Direction.Up);

        public void TranslateDown() => Translate(Direction.Down);

        public void TranslateLeft() => Translate(Direction.Left);

        public void TranslateRight() => Translate(Direction.Right);

        public enum Direction
        {
            Up,
            Down,
            Left,
            Right,
            Forward,
            Back
        }

        void Translate(Direction direction)
        {
            var transform1 = transform;
            //Verifica se a posição inicial mudou
            if (initialPosition != transform1.position - displacement)
            {
                initialPosition = transform1.localPosition;
                displacement = Vector3.zero;
            }

            //Recalcula o deslocamento da câmera
            Vector3 newDisplacement;
            switch (direction)
            {
                case Direction.Up:
                    newDisplacement = displacement + transform1.up * translateStep;
                    break;
                case Direction.Down:
                    newDisplacement = displacement - transform1.up * translateStep;
                    break;
                case Direction.Left:
                    newDisplacement = displacement - transform1.right * translateStep;
                    break;
                case Direction.Right:
                    newDisplacement = displacement + transform1.right * translateStep;
                    break;
                case Direction.Forward:
                    newDisplacement = displacement + transform1.forward * zoomStep;
                    break;
                case Direction.Back:
                    newDisplacement = displacement - transform1.forward * zoomStep;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
            
            //Verifica as pré-condições
            var deslocamentoMaiorQueRaio = Vector3.Magnitude(newDisplacement) > maximumDisplacementRadius;
            var novoDeslocamentoDiminuiRaio = Vector3.Magnitude(newDisplacement) < Vector3.Magnitude(displacement);
            if (!deslocamentoMaiorQueRaio || novoDeslocamentoDiminuiRaio)
            {
                displacement = newDisplacement;
                transform1.localPosition = initialPosition + displacement;
            }
        }
    }
}