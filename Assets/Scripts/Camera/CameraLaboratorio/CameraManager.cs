﻿using System.Collections;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Total de câmeras principais")]
    int cameraQuantity;

    bool AltPressed = false;
    int _currentCameraIndex = 0;
    public int initialPosition;

    public CustomProfile[] customProfile;

    float clampX = 0f;

    // Lista de posições que camera deve focar ao usar a "camera rapida"
    [SerializeField] Transform[] cameraGavetaTransform;
    IEnumerator moveCameraCoroutine;
    IEnumerator rotateCameraCoroutine;

    class SingletonCreator
    {
        static SingletonCreator()
        {
        }

        public static CameraManager instance;
    }

    public static CameraManager UniqueInstance => SingletonCreator.instance;

    CameraManager()
    {
    }

    void Awake()
    {
        SingletonCreator.instance = this;
    }

    void Start()
    {
        _currentCameraIndex = initialPosition;
        IndexMoveCamera(_currentCameraIndex);
    }

    void HotKey()
    {
        for (var i = 1; i < cameraQuantity + 1; i++)
            if (Input.GetKeyDown(i.ToString("0")))
            {
                if (i > cameraGavetaTransform.Length)
                    return;
                IndexMoveCamera(i - 1);
            }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt)) AltPressed = true;
        if (Input.GetKeyUp(KeyCode.LeftAlt)) AltPressed = false;
        if (AltPressed) HotKey();
    }

    public void IndexMoveCamera(int index)
    {
        IndexMoveCamera(index, 20f, 2f);
    }

    public void IndexMoveCamera(int index, float speed, float duration) //Usado para ser associado a botões, chama scripts determinando qual posição a câmera deve ir
    {
        _currentCameraIndex = index;
        MoveCameraToPosition(index, speed, duration);
        CameraLookAt(index);

    }

    void MoveCameraToPosition(int index, float speed, float duration) // translada a camera para a posição da lista baseada no index
    {
        /* if (cameraGavetaTransform[index].gameObject.GetComponent<CameraOrtographic>())
        {
            gameObject.GetComponent<Camera>().orthographic = true;
            gameObject.GetComponent<Camera>().orthographicSize =
                cameraGavetaTransform[index].gameObject.GetComponent<CameraOrtographic>().size;
        }
        else
        {
            gameObject.GetComponent<Camera>().orthographic = false;
            gameObject.GetComponent<Camera>().fieldOfView = 60;
        } */

        if (moveCameraCoroutine != null) StopCoroutine(moveCameraCoroutine);

        if (rotateCameraCoroutine != null) StopCoroutine(rotateCameraCoroutine);

        moveCameraCoroutine = MoveCamera(index, speed, duration);
        rotateCameraCoroutine = RotateCamera(index, speed, duration);

        StartCoroutine(moveCameraCoroutine);
        StartCoroutine(rotateCameraCoroutine);
        //transform.position = cameraGavetaTransform [index].position;
    }

    IEnumerator MoveCamera(int index, float speed, float duration)
    {
        var initialPos = transform.position;
        var finalPos = cameraGavetaTransform[index].position;
        var frameQuantity = Mathf.FloorToInt(duration * speed);
        YieldInstruction wait = new WaitForSeconds(1f / speed);
        for (float i = 0; i < frameQuantity; i++)
        {
            transform.position = Vector3.Lerp(initialPos, finalPos, i / (frameQuantity - 1));
            yield return wait;
        }
        yield return null;
    }

    void CameraLookAt(int index) //rotaciona a camera para a posição da lista baseada no index
    {
        //transform.rotation = cameraGavetaTransform [index].rotation;
        clampX = cameraGavetaTransform[index].rotation.eulerAngles.y;
    }

    IEnumerator RotateCamera(int index, float speed, float duration)
    {
        var initialRot = transform.rotation;
        var finalRot = cameraGavetaTransform[index].rotation;
        var frameQuantity = Mathf.FloorToInt(duration * speed);
        YieldInstruction wait = new WaitForSeconds(1f / speed);
        for (float i = 0; i < frameQuantity; i++)
        {
            transform.rotation = Quaternion.Slerp(initialRot, finalRot, i / (frameQuantity - 1));
            yield return wait;
        }
        yield return null;
    }
}

[System.Serializable]
public class CustomProfile
{
    public bool enable;

}