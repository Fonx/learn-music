﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Camera2Command : MonoBehaviour
{
    public Transform targetTransform;

    // ROTAÇÃO
    public float rotateAroundSpeed = 100;
    public float rotateOverSpeed = 50;

    // ZOOM
    public float zoomSpeed = 1;

    // VERTICAL PAN
    public float verticalSpeed = 1;
    // HORIZONTAL PAN
    public float horizontalSpeed = .5f;

    // REFERENCIAS DE SCRIPTS
    Camera2RotateAround rotateCameraAround;
    Camera2RotateOver rotateCameraOver;
    Camera2Zoom cameraZoom;
    Camera2VerticalPan verticalPanCamera;
    Camera2HorizontalPan horizontalPan;

    public static UnityAction<bool> cameraRotateEvent, cameraPannEvent;
    private bool isRotating;
    public bool IsRotating
    {
        get => isRotating;
        set
        {
            if (isRotating != value)
            {
                isRotating = value;
                if (cameraRotateEvent != null) cameraRotateEvent.Invoke(value);
            }
        }
    }
    private bool onVerticalPan;
    public bool OnVerticalPan
    {
        get => onVerticalPan;
        set
        {
            if (onVerticalPan != value)
            {
                onVerticalPan = value;
                if (cameraPannEvent != null) cameraPannEvent.Invoke(value);
            }
        }
    }

    // INICIALIZAÇÃO
    private void Awake()
    {
        rotateCameraAround = GetComponent<Camera2RotateAround>();
        rotateCameraOver = GetComponent<Camera2RotateOver>();
        cameraZoom = GetComponent<Camera2Zoom>();
        verticalPanCamera = GetComponent<Camera2VerticalPan>();
        horizontalPan = GetComponent<Camera2HorizontalPan>();
    }

    private void Start()
    {
        isRotating = false;
        onVerticalPan = false;
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Mouse1)) { StopRotating(); }     //STOP ROTATE AROUND
        if (Input.GetKeyUp(KeyCode.Mouse2)) { StopVerticalPann(); StopHorizontalPan(); }

        //transform.LookAt(targetTransform);

        // ROTATE CONTROL
        if (Input.GetKeyDown(KeyCode.Mouse1)) { StartRotating(1); }  //ROTATE AROUND

        // ZOOM CONTROL
        if (Input.mouseScrollDelta.y != 0) { StartZoom(1); }         // START ZOOM IN OR OUT

        // VERTICAL PAN
        if (Input.GetKeyDown(KeyCode.Mouse2)) { VerticalPann(1); HorizontalPan(1); }
    }

    public void StartRotating(float direction)
    {
        IsRotating = true;
        rotateCameraAround.RotateCameraAroundTarget(targetTransform.position, -rotateAroundSpeed * direction);
        rotateCameraOver.RotateCameraOverTarget(targetTransform.position, rotateOverSpeed * direction);
    }
    public void StopRotating()
    {
        IsRotating = false;
        rotateCameraAround.StopRotating();
        rotateCameraOver.StopRotating();
    }
    public void StartZoom(float direction)
    {
        cameraZoom.Zoom(Input.mouseScrollDelta.y, zoomSpeed * direction, targetTransform.position);
    }

    public void VerticalPann(float direction)
    {
        OnVerticalPan = true;
        verticalPanCamera.MoveVertical(targetTransform, verticalSpeed * direction);
    }
    public void StopVerticalPann()
    {
        OnVerticalPan = false;
        verticalPanCamera.StopPan();
    }

    public void HorizontalPan(float direction)
    {
        //OnHorizontalPan = true;
        horizontalPan.MoveHorizontal(targetTransform, horizontalSpeed * direction);
    }
    public void StopHorizontalPan()
    {
        //OnHorizontalPan = false;
        horizontalPan.StopPan();
    }

    // COMANDOS POR UI

    public void VerticalPannUI(float direction)
    {
        verticalPanCamera.MovingByUI(targetTransform, verticalSpeed, direction);
    }
    public void StartHorizontalPanUI(float direction)
    {
        horizontalPan.MovingByUI(targetTransform, horizontalSpeed, direction);
    }
    public void StartRotateUI(float direction)
    {
        rotateCameraAround.RotateCameraAroundTargetUI(targetTransform.position, rotateAroundSpeed, direction);
    }
    public void StartRotateOverUI(float direction)
    {
        rotateCameraOver.RotateCameraOverTargetUI(targetTransform.position, rotateOverSpeed, direction);
    }
    public void StartZoomUI(float direction)
    {
        cameraZoom.ZoomUI(direction, zoomSpeed, targetTransform.position);
    }
    public void StopZoomUI()
    {
        cameraZoom.StopZoomUI();
    }
}
