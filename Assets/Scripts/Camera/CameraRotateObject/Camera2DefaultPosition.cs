﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2DefaultPosition : MonoBehaviour
{
    public Transform cameraTransform;
    public Transform targetTransform;

    public float velocity;

    private Vector3 initCameraPosition;
    private Vector3 initTargetPosition;
    private bool setting = false;
    private IEnumerator coroutine;
    private void Awake()
    {
        initCameraPosition = cameraTransform.position;
        initTargetPosition = targetTransform.position;
    }

    public void SetCameraToDefault()
    {
        if (setting) return;
        coroutine = SetCameraToDefaultPosition();
        StartCoroutine(coroutine);
    }

    //Desativar a corroutine que moviment a camera para o default quando o usuario quiser novamente interagir com a cena
    private void Update()
    {
        if (setting)
        {
            if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                if (coroutine != null)
                {
                    setting = false;
                    StopCoroutine(coroutine);
                }
            }     //STOP ROTATE AROUND
            if (Input.GetKeyUp(KeyCode.Mouse2))
            {
                if (coroutine != null)
                {
                    setting = false;
                    StopCoroutine(coroutine);
                }
            }

            // ROTATE CONTROL
            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                if (coroutine != null)
                {
                    setting = false;
                    StopCoroutine(coroutine);
                }
            }  //ROTATE AROUND

            // ZOOM CONTROL
            if (Input.mouseScrollDelta.y != 0)
            {
                if (coroutine != null)
                {
                    setting = false;
                    StopCoroutine(coroutine);
                }
            }         // START ZOOM IN OR OUT

            // VERTICAL PAN
            if (Input.GetKeyDown(KeyCode.Mouse2))
            {
                if (coroutine != null)
                {
                    setting = false;
                    StopCoroutine(coroutine);
                }
            }
        }
    }

    IEnumerator SetCameraToDefaultPosition()
    {
        setting = true;
        float i = 0;
        while (i <= 1)
        {
            cameraTransform.position = Vector3.Slerp(cameraTransform.position, initCameraPosition, i);
            targetTransform.position = Vector3.Slerp(targetTransform.position, initTargetPosition, i);
            cameraTransform.LookAt(targetTransform.position);
            i += velocity;
            yield return null;
        }
        setting = false;

        yield return null;
    }
}
