﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2HorizontalPan : MonoBehaviour
{
    public float mouseOffset;
    public float maxHeight;
    public float minHeight;
    float horizontalPanSpeed;
    private Vector2 originMousePosition;
    private Transform targetTransform;
    private float direction = 0;
    bool isInspecting = false;


    public void MoveHorizontal(Transform newTargetTransform, float horizontalSpeed)
    {
        originMousePosition = Input.mousePosition;
        targetTransform = newTargetTransform;
        horizontalPanSpeed = horizontalSpeed;
        //if (!isInspecting) //Comentado para permitir uso de vertical pan no inspecionar
        InvokeRepeating("PanDirection", 0, 0.01f);
    }
    public void StopPan()
    {
        CancelInvoke();
    }
    public void MovingByUI(Transform newTargetTransform, float horizontalSpeed, float Newdirection)
    {
        originMousePosition = Input.mousePosition;
        targetTransform = newTargetTransform;
        horizontalPanSpeed = horizontalSpeed;
        direction = Newdirection;
        InvokeRepeating("Moving", 0, 0.01f);
    }
    void PanDirection()
    {
        float dir = Input.mousePosition.x - originMousePosition.x;
        if (mouseOffset > Mathf.Abs(dir))
            direction = 0;
        else
        {
            direction = (dir - mouseOffset * (Mathf.Sign(dir))) / Screen.width;
            Moving();
        }


    }
    void Moving()
    {
        float increment = horizontalPanSpeed * direction;
        transform.position += transform.right * increment * CheckLimits(direction, increment);
        targetTransform.position += transform.right.normalized * increment * CheckLimits(direction, increment);
    }

    float CheckLimits(float dir, float increment)
    {
        float magnitude =
            (
            new Vector2(transform.localPosition.x, transform.localPosition.z) +
            new Vector2(transform.localPosition.x, transform.localPosition.z).normalized * increment
            ).magnitude;
        if (magnitude > maxHeight)
        {
            return 0;
        }
        return 1;
    }
}
