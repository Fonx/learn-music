﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2RotateAround : MonoBehaviour
{
    private float rotateSpeed = 100;
    private Vector3 targetPosition;
    private Vector2 originMousePosition;

    public float direction = 1;

    public void RotateCameraAroundTarget(Vector3 newTargetPosition, float newRotateSpeed)
    {
        rotateSpeed = newRotateSpeed;
        targetPosition = newTargetPosition;
        originMousePosition = Input.mousePosition;
        GetVelocity();
    }
    public void RotateCameraAroundTargetUI(Vector3 newTargetPosition, float newRotateSpeed, float newDirection)
    {
        direction = newDirection;
        rotateSpeed = newRotateSpeed;
        targetPosition = newTargetPosition;
        originMousePosition = Input.mousePosition;
        InvokeRepeating("RotateByUI", 0, 0.01f);
    }
    void RotateByUI()
    {
        transform.RotateAround(targetPosition, Vector3.up, rotateSpeed * direction * Time.deltaTime);
    }
    void GetVelocity()
    {
        InvokeRepeating("RotateAroundTheTarget", 0, 0.01f);
    }

    void RotateAroundTheTarget()
    {
        float a = Input.mousePosition.x - originMousePosition.x;
        a = a / Screen.width;

        transform.RotateAround(targetPosition, Vector3.up, rotateSpeed * a * Time.deltaTime);
    }
    public void StopRotating()
    {
        CancelInvoke();
    }
}
