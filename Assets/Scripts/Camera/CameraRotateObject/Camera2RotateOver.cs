﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2RotateOver : MonoBehaviour
{
    public float test;
    public float maxAngle;
    public float minAngle;
    private float rotateSpeed = 100;
    private Vector3 targetPosition;
    private Vector2 originMousePosition;
    private float direction;

    public void RotateCameraOverTarget(Vector3 newTargetPosition, float newRotateSpeed)
    {
        rotateSpeed = newRotateSpeed;
        targetPosition = newTargetPosition;
        originMousePosition = Input.mousePosition;
        GetVelocity();
    }
    public void RotateCameraOverTargetUI(Vector3 newTargetPosition, float newRotateSpeed, float newDirection)
    {
        direction = newDirection;
        rotateSpeed = newRotateSpeed;
        targetPosition = newTargetPosition;
        originMousePosition = Input.mousePosition;
        InvokeRepeating("RotateOverTheTargetUI", 0, 0.01f);
    }

    void GetVelocity()
    {
        InvokeRepeating("RotateOverTheTarget", 0, 0.01f);
    }

    void RotateOverTheTarget()
    {
        float dir = Input.mousePosition.y - originMousePosition.y;
        float b = dir / Screen.width;
        float increment = rotateSpeed * b * Time.deltaTime;
        transform.RotateAround(targetPosition, transform.right, increment);

        var targetRotation = Quaternion.LookRotation(targetPosition - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime);
    }

    void RotateOverTheTargetUI()
    {
        float increment = rotateSpeed * direction * Time.deltaTime;
        transform.RotateAround(targetPosition, transform.right, increment);

    }
    public void StopRotating()
    {
        CancelInvoke();
    }
}
