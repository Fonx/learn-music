﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2VerticalPan : MonoBehaviour
{
    public float maxHeight;
    public float minHeight;
    float verticalPanSpeed;
    private Vector2 originMousePosition;
    private Transform targetTransform;
    private float direction = 0;
    bool isInspecting = false;

    public void MoveVertical(Transform newTargetTransform, float verticalSpeed)
    {
        originMousePosition = Input.mousePosition;
        targetTransform = newTargetTransform;
        verticalPanSpeed = verticalSpeed;
        //if (!isInspecting) //Comentado para permitir uso de vertical pan no inspecionar
        InvokeRepeating("PanDirection", 0, 0.01f);
    }
    public void StopPan()
    {
        CancelInvoke();
    }
    public void MovingByUI(Transform newTargetTransform, float verticalSpeed, float Newdirection)
    {
        originMousePosition = Input.mousePosition;
        targetTransform = newTargetTransform;
        verticalPanSpeed = verticalSpeed;
        direction = Newdirection;
        InvokeRepeating("Moving", 0, 0.01f);
    }
    void PanDirection()
    {
        float dir = Input.mousePosition.y - originMousePosition.y;
        direction = dir / Screen.width;
        Moving();
    }
    void Moving()
    {
        float increment = verticalPanSpeed * direction;
        transform.position += Vector3.up * increment * CheckLimits(direction, increment);
        targetTransform.position += Vector3.up * increment * CheckLimits(direction, increment);
    }

    float CheckLimits(float dir, float increment)
    {
        if (transform.position.y + increment > maxHeight && dir > 0)
        {
            return 0;
        }
        if (transform.position.y - increment < minHeight && dir < 0)
        {
            return 0;
        }
        return 1;
    }
}
