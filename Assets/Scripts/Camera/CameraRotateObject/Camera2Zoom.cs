﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2Zoom : MonoBehaviour
{
    public float offsetFromTarget;
    public float maxDistanceFromTarget;
    private Vector3 originPosition;
    private Vector3 targetPosition;

    float direction = 0;
    float zoomSpeed = 0;

    private void Awake()
    {
        originPosition = transform.position;
    }

    public void Zoom(float newDirection, float newZoomSpeed, Vector3 newTargetPosition)
    {
        targetPosition = newTargetPosition;
        direction = newDirection;
        zoomSpeed = newZoomSpeed;
        StartZoom(direction, zoomSpeed);
    }

    void StartZoom(float direction, float zoomSpeed)
    {
        float increment = zoomSpeed * direction * Time.deltaTime;
        transform.position += transform.forward * increment * CheckLimits(direction, increment);
    }

    public void ZoomUI(float newDirection, float newZoomSpeed, Vector3 newTargetPosition)
    {
        targetPosition = newTargetPosition;
        direction = newDirection;
        zoomSpeed = newZoomSpeed;
        InvokeRepeating("StartZoomUI", 0, 0.01f);
    }
    public void StopZoomUI()
    {
        CancelInvoke();
    }
    void StartZoomUI()
    {
        float increment = zoomSpeed * direction * Time.deltaTime;
        transform.position += transform.forward * increment * CheckLimits(direction, increment);
    }


    float CheckLimits(float dir, float increment)
    {
        float distanceToTarget = (targetPosition - transform.forward * offsetFromTarget - transform.position).magnitude;
        if (dir > 0)
        {
            if (increment > distanceToTarget)
            {
                transform.position = targetPosition - transform.forward * offsetFromTarget;
                return 0;
            }
            return 1;
        }
        else
        {
            if (distanceToTarget > maxDistanceFromTarget)
            {
                transform.position = originPosition - transform.forward.normalized * maxDistanceFromTarget;
                return 0;
            }
            return 1;
        }
    }
}
