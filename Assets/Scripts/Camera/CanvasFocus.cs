﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CanvasGroup))]
public class CanvasFocus : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler 
{
	private CanvasGroup canvasGroup;
	public float withFocus = 1f, withoutFocus = 0.5f;
	public bool startVisible = true;
	private void Start()
	{
		canvasGroup = gameObject.GetComponent<CanvasGroup>();
		StartCoroutine(WaitLoading(1f));
	}
	public void OnPointerEnter(PointerEventData eventData)
	{
		canvasGroup.alpha = withFocus;
	}
	public void OnPointerExit(PointerEventData eventData)
	{
		canvasGroup.alpha = withoutFocus;
	}
	private IEnumerator WaitLoading(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		if(startVisible) canvasGroup.alpha = withoutFocus;
	}
}
