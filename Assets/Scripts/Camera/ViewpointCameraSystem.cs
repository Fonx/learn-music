﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class ViewpointCameraSystem : MonoBehaviour
{
    public Camera mainCamera, cameraXY;
    [HideInInspector] public bool isFlashcardOpen;
    private Rect rect;
    public Color[] colors;
    private bool inTransition;
    const int frames = 24;
    const int frameDelay = 20; //em ms
    const float amplitude = 0.35f;

    private void Start()
    {
        isFlashcardOpen = false;
        rect = mainCamera.rect;
        inTransition = false;
    }
    private void OnEnable()
    {
        BackgroundColorControl.ColorChange += UpdateBackgroundColor;

    }

    private void OnDisable()
    {
        BackgroundColorControl.ColorChange -= UpdateBackgroundColor;

    }

    public void InspectMode()
    {
        if (inTransition) return;
        isFlashcardOpen = !isFlashcardOpen;

        if (isFlashcardOpen) OnInspectMode();
        if (!isFlashcardOpen) OffInspectMode();
    }

    void UpdateBackgroundColor(int i)
    {
        cameraXY.backgroundColor = colors[i];
    }

    public void OnInspectMode()
    {
        inTransition = true;
        cameraXY.transform.gameObject.SetActive(true);
        //r.position = new Vector2(0.2f, 0.2f);
        //mainCamera.rect = r;

        /*         for (int i = 0; i < frames; i++)
                {
                    rect.position = Vector2.right * (i + 1) * amplitude * (1f / (float)frames);
                    mainCamera.rect = rect;
                    await Task.Delay(frameDelay);
                } */

        rect.position = new Vector2(0.2f, 0);
        mainCamera.rect = rect;

        inTransition = false;

    }
    public void OffInspectMode()
    {
        inTransition = true;
        /*         for (int i = frames - 1; i >= 0; i--)
                {
                    rect.position = Vector2.right * (i + 1) * amplitude * (1f / (float)frames);
                    mainCamera.rect = rect;
                    await Task.Delay(frameDelay);
                } */

        //Force Final Position
        rect.position = Vector2.zero;
        mainCamera.rect = rect;

        cameraXY.transform.gameObject.SetActive(false);
        inTransition = false;

    }

    void InspectChange(bool state)
    {
        if (state == false && isFlashcardOpen)
        {
            OffInspectMode();
            isFlashcardOpen = false;
        }
    }

}
