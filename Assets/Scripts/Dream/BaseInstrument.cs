﻿//using System.Collections.Generic;
//using UnityEngine;

///*
//    Exemplo de instrumento contendo um único audio source (uma flauta).
//    Boa parte desta classe pode ser reimplementada para demais instrumentos simples (ou seja, não harmônicos),
//    uma vez que possuem apenas uma única audio source. Um piano possui diversas audios sources (cada tecla tem uma).
//    Lembrar de modificar e olhar sempre os seguintes métodos:
//    PlayPlayback() -> Responsável por fazer o playback da musica
//    MapPlaybackNotes() -> Responsável por mapear as notas do instrumento para facilitar o playback
//    Play() -> Responsável por deixar o instrumento tocavel
//*/
//namespace Dream
//{
//    [RequireComponent(typeof(AudioSource))]
//    public class BaseInstrument : MonoBehaviour, IInstrument
//    {
//        [SerializeField] private AudioSource audioSource;
//        [SerializeField] private Note[] notes;

//        private int currentNoteOnTrack;
//        private float currentTime;
//        private float newTime;

//        [SerializeField] private Music currentTrack;
//        private bool isPlaying; //Play = Soprada ou tocada
//        [SerializeField] private bool isPlayingBack;

//        public Music Track
//        {
//            get
//            {
//                return currentTrack;
//            }

//            set
//            {
//                if (currentTrack != value)
//                {
//                    currentTrack = value;
//                }
//            }
//        }

//        public bool IsPlaying
//        {
//            get
//            {
//                return isPlaying;
//            }
//        }

//        /// <summary>
//        /// PlayPlayback() e StopPlayBack(). PlayPlayback() começa a tocar a track. StopPlayBack() para de tocar a track (resultado, dar Play novamente faz começar do 0).
//        /// </summary>
//        /// <value></value>
//        public bool IsPlayingBack
//        {
//            get
//            {
//                return isPlayingBack;
//            }

//            set
//            {
//                isPlayingBack = value;

//                if (isPlayingBack)
//                {
//                    isPlaying = false;
//                }
//                else
//                {
//                    currentNoteOnTrack = 0;
//                    currentTime = 0f;
//                    newTime = 0f;
//                }
//            }
//        }

//        /// <summary>
//        /// Progresso do playback. 0 a 1. -1 se a music de playback for null.
//        /// </summary>
//        /// <value></value>
//        public float PlaybackProgress
//        {
//            get
//            {
//                if (currentTrack != null)
//                {
//                    if (!currentTrack.UseAudioClip)
//                    {
//                        return (float)currentNoteOnTrack / currentTrack.notes.Length;
//                    }
//                    else
//                    {
//                        if (audioSource.clip == null)
//                        {
//                            Debug.Log("Audiosource Clip equals null. Object : " + gameObject.name);
//                            return -1f;
//                        }

//                        return (float)audioSource.time / audioSource.clip.length;
//                    }
//                }

//                Debug.Log("Track equals null. Object : " + gameObject.name);

//                return -1f;
//            }
//        }

//        private Dictionary<Note.Notes, Note> mappedNotes;

//        private void Start()
//        {
//            Debug.Log(gameObject.name);
//            currentNoteOnTrack = 0;
//            currentTime = 0f;
//            newTime = 0f;

//            mappedNotes = new Dictionary<Note.Notes, Note>();
//            MapPlaybackNotes();
//        }

//        /// <summary>
//        /// Mapeia as notas dada o enum.Notas, para facilitar o playback.
//        /// </summary>
//        private void MapPlaybackNotes()
//        {
//            for (int i = 0; i < notes.Length; i++)
//            {
//                mappedNotes.Add(notes[i].CurrentNote, notes[i]);
//            }
//        }

//        void OnGUI()
//        {
//            if (GUI.Button(new Rect(10, 10, 150, 100), "Playback"))
//            {
//                isPlayingBack = true;
//            }
//        }

//        public void FixedUpdate()
//        {
//            PlayPlayback();
//        }

//        //Triste mas é isso
//        private void PlayPlayback()
//        {
//            if (isPlayingBack && !isPlaying)
//            {
//                if (currentTrack == null)
//                {
//                    Debug.Log("Track equals null. Object : " + gameObject.name);
//                    isPlayingBack = false;
//                    return;
//                }

//                if (!currentTrack.UseAudioClip)
//                {
//#if UNITY_EDITOR
//                    Debug.Log("CurrentTime : " + currentTime);
//                    Debug.Log("New Note Time : " + newTime);
//#endif

//                    currentTime += Time.fixedDeltaTime;

//                    if (currentTime > newTime)
//                    {
//                        if (currentNoteOnTrack < currentTrack.notes.Length)
//                        {
//                            mappedNotes[currentTrack.notes[currentNoteOnTrack].Note].Octave = currentTrack.notes[currentNoteOnTrack].Octave;
//                            mappedNotes[currentTrack.notes[currentNoteOnTrack].Note].Play();
//                            newTime = currentTime + currentTrack.notes[currentNoteOnTrack].Time; //Time em segundos
//                            currentNoteOnTrack++;
//                        }
//                        else
//                        {
//                            audioSource.Stop();
//                            IsPlayingBack = false;
//                        }
//                    }
//                }
//                else
//                {
//                    if (!audioSource.isPlaying)
//                    {
//                        audioSource.clip = currentTrack.audioClip;
//                        audioSource.Play();
//                    }

//                    if (currentTime > currentTrack.audioClip.length)
//                    {
//                        isPlayingBack = false;
//                        audioSource.Stop();
//                    }
//                }

//            }
//            else
//            {
//                if (audioSource.isPlaying)
//                {
//                    audioSource.Stop();
//                }
//            }
//        }

//        //TODO identificar nota
//        public virtual void Play() //Uma "tocada" no violão. Uma "soprada" na flauta
//        {

//        }

//        public void SeekPlayback(float time)
//        {
//            if (time < 0 || time > 1)
//            {
//                return;
//            }

//            if (currentTrack != null)
//            {
//                if (currentTrack.UseAudioClip)
//                {
//                    if (audioSource.clip != null)
//                    {
//                        audioSource.time = Mathf.Lerp(0, audioSource.clip.length, time);
//                    }
//                    else
//                    {
//                        Debug.Log("Audiosource Clip equals null. Object : " + gameObject.name);
//                    }
//                }
//                else
//                {
//                    currentNoteOnTrack = (int)Mathf.Lerp(0, currentTrack.notes.Length, time);
//                }
//            }
//            else
//            {
//                Debug.Log("Track equals null. Object : " + gameObject.name);
//            }
//        }

//        /// <summary>
//        /// Permite que você continue a música a partir da posição atual
//        /// </summary>
//        public void ResumePlayback()
//        {
//            isPlayingBack = true;
//        }

//        /// <summary>
//        /// Permite que você pare a música na posição atual
//        /// </summary>

//        public void PausePlayback()
//        {
//            isPlayingBack = false;
//        }
//    }
//}
