﻿using Dream;

public interface IInstrument
{
    Music Track { get; set; } //Capacidade de selecionar e obter a track atual
    bool IsPlaying { get; } //Tocada ou soprada da flauta
    bool IsPlayingBack { get; set; } //Se o playback está ocorrendo
    float PlaybackProgress { get; } //Progresso atual do playback

    /// <summary>
    /// Representa uma "tocada" no violão. Uma "soprada" na flauta e por ai vai.
    /// </summary>
    void Play();

    /// <summary>
    /// Resume na nota/tempo atual, podendo pausar a partir daqui.
    /// </summary>
    void ResumePlayback();

    /// <summary>
    /// Para na nota/tempo atual, podendo resumir a partir daqui.
    /// </summary>
    void PausePlayback();

    /// <summary>
    /// Utilize isto para fazer o playback. Aqui você tem um controle de tempo melhor e um consumo bem menor (uma vez que é inato ao Unity)
    /// </summary>
    void FixedUpdate();

    /// <summary>
    /// Percorrer o tempo do playback.
    /// </summary>
    /// <param name="time">Entre 0 e 1. 0 representa o inicio, 1 o final.</param>
    void SeekPlayback(float time);

    //Representados pelo bool IsPlayingBack
    //void PlayPlayback(); //Playback da musica
    //void StopPlayback();
}
