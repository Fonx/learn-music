﻿using UnityEngine;

namespace Dream
{
    [CreateAssetMenu(fileName = "Music", menuName = "ScriptableObjects/Music/Music", order = 2)]
    public class Music : ScriptableObject
    {
        [SerializeField]
        private string musicName;
        public float Width;
        public AudioClip audioClip;
        public AudioClip acompanhamento;
        public AudioClip solo;
        public AudioClip audioClipLento;
        public PlaybackNote[] notes;//Talvez um array de array de playbacknotes para instrumentos harmonicos ?
        public Sprite partiture;
        public float initialPos;
        public float bpm;
        [SerializeField]
        private bool useAudioClip = false;

        public bool UseAudioClip
        {
            get
            {
                return useAudioClip;
            }
        }

        public string MusicName
        {
            get
            {
                return musicName;
            }
        }
    }
}