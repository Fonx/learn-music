﻿using UnityEngine;

/*
    Pode ser utilizado para representar apenas uma única nota e suas variações
    ou várias notas. Ou seja, pode representar uma corda inteira do violão
    (aí, a mudança no inteiro Octave seria mudar a fresta apertada)
    ou apenas uma única nota (a fundamental) com seus oitavos.
    Isto permite a construção em si do instrumento (o mais chato) de forma mais livre ;~; 
    Utilizado na cena para interação do estudante com o instrument.
*/
namespace Dream
{
    public class Note : MonoBehaviour
    {
        [SerializeField] private Notes currentNote;
        public Notes CurrentNote
        {
            get
            {
                return currentNote;
            }
        }

        [SerializeField] private AudioSource audioSource; //Audio source do instrumento ou própria. Exemplo do instrumento: flauta ; Própria: Piano

        [SerializeField]
        [Tooltip("Octaves[0] = nota padrão.")]
        private AudioClip[] octaves;//arrumar outro nome dps
        private static int whichOctave;
        public int Octave
        {
            get
            {
                return whichOctave;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                    Debug.Log("Value < 0");
                }
                else if (value > octaves.Length)
                {
                    value = octaves.Length - 1;

                    if (value < 0)
                    {
                        value = 0;
                    }

                    Debug.Log("Value > lenght");
                }

                whichOctave = value;
            }
        }

        private void Awake()
        {
            if (audioSource == null)
            {
                Debug.LogWarning("Audio Source equals null. Object :" + gameObject.name);
            }
        }

        public void Play()
        {
            if (audioSource == null)
            {
                Debug.LogWarning("Audio Source equals null. Object :" + gameObject.name);
                return;
            }

            //Nota para timing apenas
            if (currentNote == Notes.Void)
            {
                return;
            }

            audioSource.clip = octaves[whichOctave];
            audioSource.Play();
        }

        //Apenas informativo
        //s = Sustenido
        public enum Notes { C, Cs, D, Ds, E, F, Fs, G, Gs, A, As, B, Void }
        //Void Note é uma nota vazia, apenas para dar um timing sem som entre notas ;~;
    }
}