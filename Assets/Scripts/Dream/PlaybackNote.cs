﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlaybackNote", menuName = "ScriptableObjects/Music/Playbacknote", order = 1)]
public class PlaybackNote : ScriptableObject
{
    [SerializeField]
    private Note.Notes note;
    [SerializeField]
    private int octave = 0; // aqui é sempre um a mais, pois é a oitava real mid
    [SerializeField]
    private float time = 1f; //1 segundo ;~; deve tá bem errado aqui
    [SerializeField]
    private int passoPicture;
    public Note.Notes Note
    {
        get
        {
            return note;
        }
    }

    public int Octave
    {
        get
        {
            return octave;
        }
    }

    public float Time
    {
        get
        {
            return time;
        }
    }

    public float Passo
    {
        get
        {
            return passoPicture;
        }
    }
}

