﻿using System;
using UnityEngine;
using TMPro;
using System.Collections.Generic;
using System.Globalization;

/// <summary>
/// Afinador eletrônico. Só funciona em conexão com HTTPS.
/// </summary>
public class Tuner : MonoBehaviour
{
    private float speed = 1f;
    private Quaternion newRotation;
    public int qtdAmostras;
    public List<float> cents;
    [Header("Detune")]
    [SerializeField] private RectTransform neddleTransform;
    [SerializeField] private RectTransform midDetuneTransform;
    [SerializeField] private RectTransform minDetuneTransform;
    [SerializeField] private RectTransform maxDetuneTransform;

    [Space]
    [SerializeField] private int minDetune = -30;
    [SerializeField] private int maxDetune = +30;

    [Space]

    [Header("TextMeshPro")]

    [SerializeField]
    [Tooltip("[0] = Frequency ; [1] = Note ; [2] = Octave")]
    private TextMeshProUGUI[] interfaces;
    private static string Minus = "-";

    //fn = f0 * (A ^ N)
    //N = log (fn/f0) A
    //fn = A frequência da nota N passos distante da f0. Em Hz.
    //f0 = A frequência fixa de uma nota. Geralmente utiliza-se A4 = 440 Hz. Será utilizado para C0 = 16.35 Hz, pois assim, teremos apenas valores positivos para N.
    //A = Constante. (2 ^(1/12)) = 1.059463094359...
    //N = Numero de passos além da nota fixada. Se positivo, para cima, se negativo, para baixo. Dividir N (com a frequência fixa de C0) por 12 (total de notas), dá o oitavo da frequência atual.
    //https://pages.mtu.edu/~suits/NoteFreqCalcs.html
    //https://pages.mtu.edu/~suits/notefreqs.html
    private static float FixedFrequency = 16.35f; //C0 = 16.35
    private static float AConstant = Mathf.Pow(2f, (1f / 12f)); //1.059463094359...

    private void Awake()
    {
        MinusAll();
        //Debug.Log(gameObject.name);
        cents = new List<float>();
        qtdAmostras = 30;
        speed = 50f;
    }

    private void MinusAll()
    {
        for (int i = 0; i < interfaces.Length; i++)
        {
            interfaces[i].text = Minus;
        }
    }

    //Detune é utilizado para expressar quão fora do tom está a nota atual ;~; é a agulha dos afinadores que vai de -50 a +50
    //flat = negativo (-50)
    //sharp = positivo (+50)
    //Detune = (-50, 50)
    //Vide PitchDetect.js ; Testar

    // Frequency, Note, Cent, Detune (Flat/Sharp)
    /// <summary>
    /// Escreve os valores recebidos na UI do Tuner. Filtrar os valores "undefined" e "NaN".
    /// </summary>
    /// <param name="message">Separado por virgula, respectivamente: Frequency (Pitch. Em Hz), Note, Cent (Valor de detune da nota atual), Detune (Flat/Sharp). Valores "undefined" podem aparecer (filtrar esta string) </param>
    public void WriteValues(string message)
    {
        Debug.Log(message); //Remover pós testes

        var split = message.Split(',');

        if (split != null && split.Length > 0)
        {
            //Frequency
            //Octave
            try
            {
                if (split[0] != "undefined" && split[0] != "NaN")
                {
                    interfaces[0].text = split[0];
                    Debug.Log("Frequencia" + split[0]);
                    WriteOctave(float.Parse(split[0]));
                }
                else
                {
                    interfaces[1].text = "-";
                    return;
                }
            }
            catch (Exception e)
            {
                MinusAll();
            }

            //Note
            try
            {
                if (split[1] != "undefined" && split[1] != "NaN")
                {
                    interfaces[1].text = split[1];
                }
                else
                {
                    interfaces[1].text = "-";
                    //interfaces[0].text = Minus;
                    //interfaces[1].text = Minus;
                }
            }
            catch (Exception e)
            {
                MinusAll();
            }

            //Detune
            try
            {
                if (split[2] != "undefined" && split[2] != "NaN")
                {
                    //WriteDetune(float.Parse(split[2]));
                    //
                    cents.Add(float.Parse(split[2]));
                    Debug.Log("adicionando: " + float.Parse(split[2]));
                    if (cents.Count >= qtdAmostras) {
                        WriteDetune(CalculateCentAvarage(cents));
                        cents.RemoveAt(0);
                    }
                    //
                }
                else
                {
                    //interfaces[0].text = Minus;
                    //interfaces[1].text = Minus;
                    //WriteDetune(0);
                    interfaces[1].text = "-";
                }
            }
            catch (Exception e)
            {
                WriteDetune(0);
            }

        }
        else
        {
            MinusAll();
        }
    }
    public void Update()
    {
        neddleTransform.localRotation = Quaternion.RotateTowards(neddleTransform.localRotation,newRotation, speed * Time.deltaTime);
    }
    public void ChangeSpeed(TMP_InputField value) {
        speed = float.Parse(value.text, CultureInfo.InvariantCulture);
    }
    public float CalculateCentAvarage(List<float> centList) {
        float avarage = 0;
        for (int i = 0; i < centList.Count; i++) {
            avarage += centList[i];
        }
        Debug.Log(avarage / centList.Count);
        return avarage/centList.Count;
    }
    //Utilizar update para testar dps
    private void WriteDetune(float value)
    {
        if (value < 0)
        {
            newRotation = Quaternion.Lerp(minDetuneTransform.localRotation, midDetuneTransform.localRotation, (float)value / minDetune);
        }
        else if (value > 0)
        {
            newRotation = Quaternion.Lerp(midDetuneTransform.localRotation, maxDetuneTransform.localRotation, (float)value / maxDetune);
        }
        else
        {
            newRotation = midDetuneTransform.rotation;
        }
    }

    //N = log (fn/f0) A
    //Oitavo = N / 12
    //12 = total de notas
    private void WriteOctave(float frequency)
    {
        var steps = Mathf.Log((frequency / FixedFrequency), AConstant);
        interfaces[2].text = ((int)(steps / 12)).ToString();
    }
}
