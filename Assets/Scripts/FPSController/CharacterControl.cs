﻿using UnityEngine;

public class CharacterControl : MonoBehaviour {
    public float _moveSpeed;
    public float _rotateSpeed;
    public float cameraSpeed;
    private CharacterController _characterController;
    public Camera _camera;
    public CameraAttributes cameraAttributes = new CameraAttributes();

    [System.Serializable]
    public class CameraAttributes
    {
        public float yMin, yMax;
    }

    void Start ()   
    {
        //Trava ponteiro, obtém camera e controller
        //GameObject gameControllerObj = GameObject.FindGameObjectWithTag("GameController");
        //gameController = gameControllerObj.GetComponent<LabManager>();
        _characterController = GetComponent<CharacterController>();
        _camera = gameObject.GetComponentInChildren<Camera>();
        Cursor.lockState = CursorLockMode.Confined;
    }
    void Update()
    {
        MoveObjectAndCamera();
        //Se FPS e ponteiro travado, movimenta o objeto
        if (Cursor.lockState == CursorLockMode.Locked)
        {
            
        }
        //Direcionais para entrar no FPS e trava ponteiro
        else if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
        {
            //gameController.SetCursor("Crosshair");
        }
        //Escape para sair do FPS e destravar ponteiro
        //if (Input.GetKeyDown(KeyCode.Escape));
            //gameController.SetCursor("Static");
    }
    private static void LimitHeadAngle(Camera _camera, float xRot)
    {
        _camera.transform.Rotate(-xRot, 0, 0);
        float xAngle = _camera.transform.localEulerAngles.x;
        if (xAngle >= 0f && xAngle < 180f)
        {
            _camera.transform.localEulerAngles = new Vector3(Mathf.Clamp(xAngle, 0f, 70f), 0, 0);
        }
        else
        {
            _camera.transform.localEulerAngles = new Vector3(Mathf.Clamp(xAngle, 270f, 360f), 0, 0);
        }
    }
    private bool IsTheCurrentDisplayedCamera ()
    {
        return Camera.current.transform.IsChildOf(gameObject.transform);
    }
    private void MoveObjectAndCamera()
    {
        //Move personagem usando speed e escorregando entre colliders
        Vector3 moveDir = transform.forward * Input.GetAxis("Vertical") + transform.right * Input.GetAxis("Horizontal");
        _characterController.SimpleMove(moveDir * _moveSpeed);

        //MouseX rotaciona FPS Y, MouseY rotaciona camera X  
        float yRot = Input.GetAxis("Mouse X") * _rotateSpeed;
        float xRot = Input.GetAxis("Mouse Y") * _rotateSpeed;
        transform.Rotate(0, yRot, 0);
        LimitHeadAngle(_camera, xRot);

        //Abaixar/elevar a câmera com Q e E
        if (Input.GetKey(KeyCode.Q))
        {
            if (_camera.transform.localPosition.y >= cameraAttributes.yMin)
                _camera.transform.Translate(Vector3.down * Time.deltaTime * cameraSpeed, Space.World);
        }
        if (Input.GetKey(KeyCode.E))
        {
            if (_camera.transform.localPosition.y <= cameraAttributes.yMax)
                _camera.transform.Translate(Vector3.up * Time.deltaTime * cameraSpeed, Space.World);
        }
    }
}

