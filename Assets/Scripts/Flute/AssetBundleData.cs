﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Asset Bundles/Bundle Data")]
public class AssetBundleData : ScriptableObject
{
    public URLScriptableObject downloadURL;
    public string bundleName;
    public bool keepReference = true;
    public Sprite IconImage;
    public AssetBundle assetBundle;
    public string[] assetNames;
}
