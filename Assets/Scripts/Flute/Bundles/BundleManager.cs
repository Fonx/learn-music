﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.IO;
using AES;

public sealed class BundleManager : MonoBehaviour
{
    #region SINGLETON
    BundleManager() { }
    class SingletonCreator
    {
        private SingletonCreator() { }

        public static BundleManager instance;
    }

    public static BundleManager Instance
    {
        get { return SingletonCreator.instance; }
    }

    private void Awake()
    {
        if (SingletonCreator.instance == null)
            SingletonCreator.instance = this;
        else
            Destroy(gameObject);

    }
    #endregion    
    public List<AssetBundleData> LoadedHumanSystens = new List<AssetBundleData>();
    private AssetBundle assetBundle;
    //private AssetBundleData abData;
    private bool isDownload = false;
    //UnityWebRequest www;
    private AesEncrypt encryptor = new AesEncrypt();

    //private Slider loadingBar = null;
    public void DownloadAssetBundle(AssetBundleData data)
    {
        if (data.assetBundle != null)
            return;
        Slider loadingBar = null;
        AssetBundleData abData = data;
        //string downloadURL = abData.downloadURL.url + "/" + abData.bundleName + ".aes";   //criptografia
        string downloadURL = abData.downloadURL.url + "/" + abData.bundleName;              //sem criptografia

        StartCoroutine(HttpRequest(downloadURL, loadingBar, abData)); //Sends the request to download the bundle from the designated url      
    }
    public void DownloadAssetBundle(AssetBundleData data, Slider newLoadingBar)
    {
        if (data.assetBundle != null)
            return;
        Slider loadingBar = newLoadingBar;
        AssetBundleData abData = data;
        //string downloadURL = abData.downloadURL.url + "/" + abData.bundleName + ".aes";   //criptografia  
        string downloadURL = abData.downloadURL.url + "/" + abData.bundleName;              //sem criptografia

        StartCoroutine(HttpRequest(downloadURL, loadingBar, abData)); //Sends the request to download the bundle from the designated url      
    }
    public GameObject GetGameObject(AssetBundle _assetBundle, string assetName, Slider loadbar)
    {
        Debug.Log("entrou");
        if (_assetBundle == null)
        {
            Debug.LogError("Asset Bundle não carregado");
            return null;
        }

        GameObject asset = _assetBundle.LoadAsset<GameObject>(assetName);
        if (asset != null)
        {
            Debug.Log(asset.name);
            return asset;
        }
        else
        {
            Debug.LogError("Asset with name " + assetName + " was not found");
            return null;
        }
    }

    private IEnumerator HttpRequest(string downloadURL, Slider loadbar, AssetBundleData abData)
    {
        while (!Caching.ready)
        {
            yield return null;
        }

        string downloadManifestURL = abData.downloadURL.url + "/" + abData.bundleName;
        //============================================================
        var www = UnityWebRequest.Get(downloadManifestURL + ".manifest");

        // wait for load to finish
        yield return www.SendWebRequest();

        // if received error, exit
        if (www.isNetworkError == true)
        {
            Debug.LogError("www error: " + www.error);
            www.Dispose();
            www = null;
            yield break;
        }

        // create empty hash string
        Hash128 hashString = (default(Hash128));// new Hash128(0, 0, 0, 0);

        // check if received data contains 'ManifestFileVersion'
        if (www.downloadHandler.text.Contains("ManifestFileVersion"))
        {
            // extract hash string from the received data, TODO should add some error checking here
            var hashRow = www.downloadHandler.text.ToString().Split("\n".ToCharArray())[5];
            hashString = Hash128.Parse(hashRow.Split(':')[1].Trim());

            if (hashString.isValid == true)
            {
                // we can check if there is cached version or not
                if (Caching.IsVersionCached(downloadManifestURL, hashString) == true)
                {

                }
                else
                {

                }
            }
            else
            {
                // invalid loaded hash, just try loading latest bundle
                Debug.LogError("Invalid hash:" + hashString);
                yield break;
            }

        }
        else
        {
            Debug.Log("Manifest doesn't contain string 'ManifestFileVersion': " + downloadManifestURL + ".manifest");
            //yield break;
        }

        //www = UnityWebRequestAssetBundle.GetAssetBundle(downloadURL, hashString, 0);
        www.Dispose();
        //============================================================
        www = UnityWebRequest.Get(downloadURL);
        if (loadbar != null)
            StartCoroutine(GetDownloadProgress(loadbar, www));
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log("Error while downloading: " + www.error);
            www.Dispose();
            www = null;
        }
        else
        {

            //string encryptedPath = Application.streamingAssetsPath + "/Encrypted"; //criptografia
            string decryptedPath = Application.streamingAssetsPath + "/Decrypted";
            string fileName = abData.bundleName;

            /* 
            if (!Directory.Exists(encryptedPath)) // Com encriptação
            {
                Directory.CreateDirectory(encryptedPath);

            } */

            if (!Directory.Exists(decryptedPath))
            {
                Directory.CreateDirectory(decryptedPath);
            }

            //encryptedPath = encryptedPath + "/" + fileName + ".aes";
            decryptedPath = decryptedPath + "/" + fileName;

            //criptografia
            /* File.WriteAllBytes(encryptedPath, www.downloadHandler.data);
            AesEncrypt.FileDecrypt(encryptedPath, decryptedPath, "UKS9p^s9GNt]tD;A");
            File.Delete(encryptedPath); 
            */

            File.WriteAllBytes(decryptedPath, www.downloadHandler.data);
            while (!File.Exists(decryptedPath))
            {
                yield return null;
            }
            assetBundle = AssetBundle.LoadFromFile(decryptedPath);

            if (assetBundle != null)
            {
                abData.assetBundle = assetBundle; //Saves the especified bundle on the scriptable object
                abData.assetNames = assetBundle.GetAllAssetNames(); //Populates the asset names on the scriptable object
                if (abData.keepReference)
                    LoadedHumanSystens.Add(abData);
            }
            assetBundle = null;
            //Resources.UnloadUnusedAssets();
        }
    }
    public IEnumerator GetDownloadProgress(Slider loadingBar, UnityWebRequest www)
    {

        while (loadingBar.value <= 1)
        {
            loadingBar.value = www.downloadProgress;
            yield return null;
        }
        loadingBar.value = 1;
    }
}
