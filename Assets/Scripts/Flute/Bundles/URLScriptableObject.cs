﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Asset Bundles/URL")]
public class URLScriptableObject : ScriptableObject
{
    public string url;
}
