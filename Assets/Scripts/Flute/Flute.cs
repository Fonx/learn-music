﻿using UnityEngine;
using System;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class Flute : MonoBehaviour
{

    //apagar
    //private Note.Notes previousNote;
    [NonSerialized] public int selectedFluteIndex;
    [Header("Flute Gameobject")]
    [SerializeField] private MeshRenderer[] fluteRenders;
    [SerializeField] private FluteHighlightController highlightController;
    public Material[] fluteMaterials;

    [Space]

    public bool isGermanic = true; //Tipo de flauta. Germanica ou Barroca. A forma de montar algumas notas em especifico varia em cada
    [SerializeField] private AudioSource audioSource;

    [Space]

    [SerializeField]
    [Tooltip("Mandar ordenados pelo oitavo. Ou seja, primeiro oitavo e segundo oitavo respectivamente. LEMBRE-SE DE VERIFICAR O OITAVO NAS NOTAS")]
    //private Note[] notes; //Ordenar por oitavos
    private List<Note> notes; //Ordenar por oitavos
    public Transform notesTransform;
    private Hole[] holes;
    public GameObject halfHole;
    public Transform holesTransform;
    private Dictionary<string, Note> mappedNotes;
    private string currentNote; //Nota atual
    private Note.Notes enumNote;
    private int octave; //Oitavo atual
    [NonSerialized] public bool isPlaying;
    private float newTime;
    private float currentTime;
    [NonSerialized] public float currentVolume;
    //public GameObject particles;
    /// <summary>
    /// Valor entre -1, 0 e 1. 0 = Sopro ruim (vermelho), 1 = Sopro ideal (verde). -1 representa sem sopro (cinza ou branco). 
    /// </summary>
    /// <value></value>
    public float Blow
    {
        get
        {
            if (!isPlaying)
            {
                return -1f;
            }

            return currentTime / newTime;
        }
    }

    public bool IsGermanic
    {
        get
        {
            return isGermanic;
        }

        set
        {
            if (isGermanic == value)
            {
                return;
            }

            isGermanic = value;
            RefreshNotes();
        }
    }

    //public Hole.Status this[int i]
    //{
    //    get
    //    {
    //        return holes[i].status;
    //    }
    //}

    private void Awake()
    {
        //Existem 4 texturas diferentes para a flauta (Vide print com JP)
        //Podemos utilizar isso para deixar as 4 texturas
        //assim cada estudante terá uma flauta "própria" ao iniciar o lab
        //Só um adendo visual ;~;
        GenerateRandomFluteMaterial();
        GetNotes();
        MapNotes();

        InitHoles();

        audioSource.Stop();

        isPlaying = false;
        currentNote = "";

        enumNote = Note.Notes.Void;
        this.octave = -1;

        currentTime = 0;
        newTime = 0;

        //highlightController.HighLightGreen();
        //highlightController.HightLightRed();
        //highlightController.HighDont();

    }
    private void Start()
    {

    }
    public void PlayFA()
    {
        Play(Note.Notes.F, 0);
    }
    private void GenerateRandomFluteMaterial()
    {
        int random = UnityEngine.Random.Range(0, fluteMaterials.Length);
        var mat = fluteMaterials[random];
        selectedFluteIndex = random;
        ChangeFluteMaterial(mat);
    }

    public void ChangeFluteMaterial(Material mat)
    {
        for (int i = 0; i < fluteRenders.Length; i++)
        {
            fluteRenders[i].material = mat;
        }
    }
    private void InitHoles()
    {
        //holes[0] = Buraco embaixo da flauta, primeiro buraco. holes[6] e holes[7] representam os dois juntos, da esquerda para direita, assim como holes[8] e holes[9]
        //Vendo a flauta de cima, com a parte de sopro na parte superior da tela
        //https://external-content.duckduckgo.com/iu/?u=https://i.pinimg.com/originals/04/51/ca/0451cad81a0425f8b8e528c4661b19b4.jpg&f=1&nofb=1

        holes = new Hole[10];
        for (int i = 0; i < holesTransform.childCount; i++)
        {
            holes[i] = holesTransform.GetChild(i).GetComponent<Hole>();
        }

        ResetHoles();
    }

    //Reseta todos os buracos para abertos
    private void ResetHoles()
    {
        if (holes != null)
        {
            for (int i = 0; i < holes.Length; i++)
            {
                //holes[i].status = Hole.Status.Opened;
                holes[i].ChangeHoleStatus(Hole.Status.Opened);
            }
        }
    }

    /// <summary>
    /// Utilizado para dar play no audio e contar o timing.
    /// </summary>
    private void Update()
    {
        if (isPlaying)
        {
            //Primeiro clip, com o som variando
            if (!audioSource.isPlaying)
            {
                currentTime = Time.time;
                newTime = currentTime + audioSource.clip.length;
                audioSource.Play();
                return;
            }
            if (currentTime >= newTime)
            {
                //O audio em loop é para a parte final do audio (que será enviado por glaucio, cortado e separado)
                //Terá um primeiro audio, que é onde ele começa sendo soprado de maneira "ruim" até o ideal
                //e logo após, o audio com o sopro ideal ficará em loop, para o estudante ouvir
                if (!audioSource.loop)
                {
                    audioSource.clip = mappedNotes[currentNote].FinalAudioClip;
                    audioSource.Play(); //Para dar o play no clipe atual.
                    audioSource.loop = true;
                }
            }
            currentTime += Time.fixedDeltaTime;
        }
    }
    public Sprite GetSprite(Note.Notes whichNote, int octave)
    {
        string current = whichNote.ToString() + octave.ToString();
        return mappedNotes[current].NoteSprite;
    }
    //private void FixedUpdate()
    //{
    //    if (isPlaying)
    //    {
    //        if (currentTime >= newTime)
    //        {
    //            Debug.Log("AAAA");
    //            //O audio em loop é para a parte final do audio (que será enviado por glaucio, cortado e separado)
    //            //Terá um primeiro audio, que é onde ele começa sendo soprado de maneira "ruim" até o ideal
    //            //e logo após, o audio com o sopro ideal ficará em loop, para o estudante ouvir
    //            if (!audioSource.loop)
    //            {
    //                audioSource.clip = mappedNotes[currentNote].FinalAudioClip;
    //                audioSource.Play(); //Para dar o play no clipe atual.
    //                audioSource.loop = true;
    //            }
    //        }
    //        currentTime += Time.fixedDeltaTime;
    //    }
    //}

    /// <summary>
    /// Mapeia as notas para o dictionary, garantindo um acesso rápido e fácil ao audio clip correspondente
    /// </summary>
    private void GetNotes()
    {
        for (int i = 0; i < notesTransform.childCount; i++)
        {
            if (notesTransform.GetChild(i).TryGetComponent(out Note note))
            {
                notes.Add(note);
            }
        }
    }
    private void MapNotes()
    {
        mappedNotes = new Dictionary<string, Note>();
        for (int i = 0; i < notes.Count; i++)
        {
            mappedNotes.Add(notes[i].CurrentNote.ToString() + notes[i].Octave.ToString(), notes[i]);
        }
    }

    /// <summary>
    /// Responsável por dar play no audio correspondente a nota.
    /// </summary>
    /// <param name="whichNote">Enum com a nota</param>
    /// <param name="octave">Em qual oitavo. Atualmente, apenas entre 0 (representando o primeiro oitavo) e 1 (segundo oitavo).</param>
    public void Play(Note.Notes whichNote, int octave)
    {

        //particles.SetActive(true);
        //if (octave < 0 || octave > 1)
        //{
        //    Debug.Log("Octave < 0 ; Octave > 1");
        //    return;
        //}

        if (whichNote.ToString() != currentNote)
        {
            ResetHoles();
            isPlaying = false;
            audioSource.Stop();
            audioSource.loop = false;

            currentTime = 0;
            newTime = 0;

            audioSource.clip = null;

            currentNote = whichNote.ToString() + octave.ToString();
            audioSource.clip = mappedNotes[currentNote].InitialAudioClip;
            audioSource.volume = 0f;
            InvokeRepeating("FixVolume", 0, 0.07f);


            enumNote = whichNote;
            this.octave = octave;

            PressHoles(whichNote, octave);

            isPlaying = true;
        }
    }
    public void FixVolume()
    {
        audioSource.volume += 0.025f;
        if (audioSource.volume >= currentVolume)
            CancelInvoke("FixVolume");
    }
    /// <summary>
    /// Para usar como Toggle na UI
    /// </summary>
    public void TogglePauseResume()
    {
        if (isPlaying)
        {
            Pause();
        }
        else
        {
            Resume();
        }
    }

    /// <summary>
    /// Pausar a reprodução do áudio no momento atual
    /// </summary>
    public void Pause()
    {
        audioSource.Pause();
        isPlaying = false;
        //particles.SetActive(false);
    }

    /// <summary>
    /// Resumir a reprodução do áudio no momento atual
    /// </summary>
    public void Resume()
    {
        audioSource.UnPause();
        isPlaying = true;
        //particles.SetActive(true);
    }

    /// <summary>
    /// Para testar no editor as notas geradas pela flauta
    /// </summary>
#if UNITY_EDITOR
    public void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 150, 100), "Test"))
        {
            var array = (Note.Notes[])Enum.GetValues(typeof(Note.Notes));
            var randomIndex = UnityEngine.Random.Range(0, array.Length);

            PressHoles(array[randomIndex], 1);

            Debug.Log("IsGermanic = " + isGermanic);
            Debug.Log("Note = " + array[randomIndex].ToString());
            for (int i = 0; i < holes.Length; i++)
            {
                Debug.Log("Hole[" + i + "].Statues = " + holes[i].status);
            }
        }
    }
#endif

    /// <summary>
    /// Utilizado para atualizar as posições das notas devido a uma alteração no tipo da flauta
    /// </summary>
    private void RefreshNotes()
    {
        PressHoles(enumNote, this.octave);
    }

    //God may have mercy on us ;~; 
    //Imagens de referência para as notas da flauta
    //https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.pinimg.com%2Foriginals%2F04%2F51%2Fca%2F0451cad81a0425f8b8e528c4661b19b4.jpg&f=1&nofb=1
    //https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F4.bp.blogspot.com%2F-xuDCAypNcBY%2FUaNrVK_A5GI%2FAAAAAAAAADY%2FaHat_VRsrH4%2Fs1600%2Fflauta.gif&f=1&nofb=1
    public void PressHoles(Note.Notes note, int octave)
    {
        //if (previousNote != note) {
        //    Debug.Break();
       // }
       // previousNote = note;
        ResetHoles();
        if (octave == 0)
        {
            FirstOctave(note);
        }
        else if (octave == 1)
        {
            SecondOctave(note);
        }
        else
        {
            ThirdOctave(note);
        }
    }

    /// <summary>
    /// Mapeia os buracos da flauta para o primeiro oitavo, dada a nota
    /// </summary>
    private void FirstOctave(Note.Notes note)
    {
        //Os valores subtraindo de lenght se dão a partir da contagem dos buracos da flauta na ponta onde sai o som
        //Por exemplo, todos os buracos fechados = Dó
        //Se você abre um dos buracos ao final da flauta (o duplo no final), são todos os buracos - 1
        //e assim sucessivamente.
        //;~; eu poderia ter colocado tudo diretamente? poderia ;3; 
        //Seria até mais fácil de visualizar provavelmente LUL enfim
        switch (note)
        {
            case Note.Notes.C:
                {
                    for (int i = 0; i < holes.Length; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }
                    break;
                }

            case Note.Notes.Cs:
                {
                    for (int i = 0; i < holes.Length - 1; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }
                    break;
                }

            case Note.Notes.D:
                {
                    for (int i = 0; i < holes.Length - 2; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }
                    break;
                }

            case Note.Notes.Ds:
                {
                    for (int i = 0; i < holes.Length - 3; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }
                    break;
                }

            case Note.Notes.E:
                {
                    for (int i = 0; i < holes.Length - 4; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }
                    break;
                }

            case Note.Notes.F:
                {
                    if (isGermanic)
                    {
                        for (int i = 0; i < holes.Length - 5; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }
                    }
                    else
                    {
                        int i = 0;

                        for (i = 0; i < holes.Length - 5; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }

                        i++; //Skipa a posição 5, começa da 6

                        for (; i < holes.Length; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }
                    }
                    break;
                }

            case Note.Notes.Fs:
                {
                    if (isGermanic)
                    {
                        int i = 0;

                        for (; i < holes.Length - 6; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }

                        i++; //Skipa a posição 4, começa da 5

                        for (; i < holes.Length; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }
                    }
                    else
                    {
                        int i = 0;

                        for (; i < holes.Length - 6; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }

                        i++; //Skipa a posição 4, começa da 5

                        for (; i < holes.Length - 2; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }
                    }
                    break;
                }

            case Note.Notes.G:
                {
                    for (int i = 0; i < holes.Length - 6; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            case Note.Notes.Gs:
                {
                    if (isGermanic)
                    {
                        int i = 0;

                        for (; i < holes.Length - 7; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }

                        i++; //skipa posição 3, começa da 4

                        for (; i < holes.Length - 4; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }

                        i++; //Skipa posição 6, começa na 7

                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }
                    else
                    {
                        int i = 0;

                        for (; i < holes.Length - 7; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }

                        i++; //skipa posição 3, começa da 4

                        for (; i < holes.Length - 4; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }

                    }

                    break;
                }

            case Note.Notes.A:
                {
                    for (int i = 0; i < holes.Length - 7; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            case Note.Notes.As:
                {
                    int i = 0;

                    for (; i < holes.Length - 8; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    i++;

                    for (; i < holes.Length - 5; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            case Note.Notes.B:
                {
                    //SI1
                    for (int i = 0; i < holes.Length - 8; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            default:
                {
                    Debug.Log("Note doesn't exist. First Octave. Note : " + note.ToString());
                    return;
                }
        }
    }

    /// <summary>
    /// Mapeia os buracos da flauta para o segundo oitavo, dada a nota
    /// </summary>
    private void SecondOctave(Note.Notes note)
    {
        //Os valores subtraindo de lenght se dão a partir da contagem dos buracos da flauta na ponta onde sai o som
        //Se você abre um dos buracos ao final da flauta (o duplo no final), são todos os buracos - 1
        //e assim sucessivamente.
        switch (note)
        {
            case Note.Notes.C:
                {
                    //holes[0].status = Hole.Status.Pressed;
                    holes[0].ChangeHoleStatus(Hole.Status.Pressed);
                    //holes[2].status = Hole.Status.Pressed;
                    holes[2].ChangeHoleStatus(Hole.Status.Pressed);

                    break;
                }

            case Note.Notes.Cs:
                {
                    //holes[1].status = Hole.Status.Pressed;
                    holes[1].ChangeHoleStatus(Hole.Status.Pressed);
                    //holes[2].status = Hole.Status.Pressed;
                    holes[2].ChangeHoleStatus(Hole.Status.Pressed);

                    break;
                }

            case Note.Notes.D:
                {
                    //holes[2].status = Hole.Status.Pressed;
                    holes[2].ChangeHoleStatus(Hole.Status.Pressed);
                    break;
                }

            case Note.Notes.Ds:
                {
                    for (int i = 2; i < holes.Length - 2; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            case Note.Notes.E:
                {
                    holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);
                    for (int i = 1; i < holes.Length - 4; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }
                    //holes[0].status = Hole.Status.HalfPressed;
                    break;
                }

            case Note.Notes.F:
                {
                    if (isGermanic)
                    {
                        //holes[0].status = Hole.Status.HalfPressed;
                        holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);

                        for (int i = 1; i < holes.Length - 5; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }
                    }
                    else
                    {
                        //holes[0].status = Hole.Status.HalfPressed;
                        holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);

                        for (int i = 1; i < holes.Length - 5; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }

                        //holes[6].status = Hole.Status.Pressed;
                        holes[6].ChangeHoleStatus(Hole.Status.Pressed);
                        //holes[7].status = Hole.Status.Pressed;
                        holes[7].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            case Note.Notes.Fs:
                {
                    if (isGermanic)
                    {
                        //holes[0].status = Hole.Status.HalfPressed;
                        holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);

                        for (int i = 1; i < holes.Length - 6; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }

                        //holes[5].status = Hole.Status.Pressed;
                        holes[5].ChangeHoleStatus(Hole.Status.Pressed);

                        //holes[8].status = Hole.Status.Pressed;
                        holes[8].ChangeHoleStatus(Hole.Status.Pressed);
                        //holes[9].status = Hole.Status.Pressed;
                        holes[9].ChangeHoleStatus(Hole.Status.Pressed);
                    }
                    else
                    {
                        //holes[0].status = Hole.Status.HalfPressed;
                        holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);

                        for (int i = 1; i < holes.Length - 6; i++)
                        {
                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }

                        //holes[5].status = Hole.Status.Pressed;
                        holes[5].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            case Note.Notes.G:
                {
                    //holes[0].status = Hole.Status.HalfPressed;
                    holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);

                    for (int i = 1; i < holes.Length - 6; i++)
                    {
                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            case Note.Notes.Gs:
                {
                    if (isGermanic)
                    {
                        //holes[0].status = Hole.Status.HalfPressed;
                        holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);

                        for (int i = 1; i < holes.Length; i++)
                        {
                            if (i == 4)
                            {
                                continue;
                            }

                            //holes[i].status = Hole.Status.Pressed;
                            holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                        }
                    }
                    else
                    {
                        //holes[0].status = Hole.Status.HalfPressed;
                        holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);
                        //holes[1].status = Hole.Status.Pressed;
                        holes[1].ChangeHoleStatus(Hole.Status.Pressed);
                        //holes[2].status = Hole.Status.Pressed;
                        holes[2].ChangeHoleStatus(Hole.Status.Pressed);

                        //holes[4].status = Hole.Status.Pressed;
                        holes[4].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            case Note.Notes.A:
                {
                    //holes[0].status = Hole.Status.HalfPressed;
                    holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);
                    //holes[1].status = Hole.Status.Pressed;
                    holes[1].ChangeHoleStatus(Hole.Status.Pressed);
                    //holes[2].status = Hole.Status.Pressed;
                    holes[2].ChangeHoleStatus(Hole.Status.Pressed);

                    break;
                }

            case Note.Notes.As:
                {
                    //holes[0].status = Hole.Status.HalfPressed;
                    holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);

                    for (int i = 1; i < holes.Length - 2; i++)
                    {
                        if (i == 3)
                        {
                            continue;
                        }

                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            case Note.Notes.B:
                {
                    //holes[0].status = Hole.Status.HalfPressed;
                    holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);

                    for (int i = 1; i < holes.Length - 4; i++)
                    {
                        if (i == 3)
                        {
                            continue;
                        }

                        //holes[i].status = Hole.Status.Pressed;
                        holes[i].ChangeHoleStatus(Hole.Status.Pressed);
                    }

                    break;
                }

            default:
                {
                    Debug.Log("Note doesn't exist. Second Octave. Note : " + note.ToString());
                    return;
                }
        }
    }
    /// <summary>
    /// Mapeia os buracos da flauta para o terceiro oitavo, dada a nota
    /// </summary>
    private void ThirdOctave(Note.Notes note)
    {
        switch (note)
        {
            case Note.Notes.C:
                {
                    holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);
                    //holes[2].status = Hole.Status.Pressed;
                    holes[1].ChangeHoleStatus(Hole.Status.Pressed);
                    holes[4].ChangeHoleStatus(Hole.Status.Pressed);
                    holes[5].ChangeHoleStatus(Hole.Status.Pressed);

                    break;
                }

            case Note.Notes.D:
                {
                    holes[0].ChangeHoleStatus(Hole.Status.HalfPressed);
                    //holes[2].status = Hole.Status.Pressed;
                    holes[1].ChangeHoleStatus(Hole.Status.Pressed);
                    holes[3].ChangeHoleStatus(Hole.Status.Pressed);
                    holes[4].ChangeHoleStatus(Hole.Status.Pressed);

                    holes[6].ChangeHoleStatus(Hole.Status.Pressed);
                    holes[7].ChangeHoleStatus(Hole.Status.Pressed);

                    holes[8].ChangeHoleStatus(Hole.Status.Pressed);
                    break;
                }
        }
    }
}
public enum FluteType {Barroca, Germanica }
/// <summary>
/// Struct que representa o status de um buraco da flauta. Utilizado para mapear as notas na flauta
/// </summary>
//public struct Hole
//{
//    public Status status;

//    public enum Status
//    {
//        Opened, HalfPressed, Pressed
//    };
//}
