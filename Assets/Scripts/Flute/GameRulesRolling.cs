﻿using Dream;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameRulesRolling : MonoBehaviour
{
   private float previousDistanceBetweenNotes;
    public Modos modo = Modos.normal;
    public float speed = 120f;
    private float distanceBetweenNotes;
    public TMP_Text dev;
    public TMP_Text musicTitle;
    public SwapMode swapMode;
    public Image partitureImage;
    //public GameObject setaGO;
    public PlaybackNote voidNote;
    public GameObject notasPosicoes;
    public ParticleSystem particles;
    public GameObject particlesGO;
    private bool flag; //so executar pressHoles 1 vez.
    public Flute flute;
    public Music currentMusic;
    public List<Music> repertoire;
    public int index;
    public int indexMusic;
    public AudioSource audioSource;
    public float currentTime;
    private bool isPlaying;
    private bool isPaused;
    public TMP_Text noteText;

    void Start()
    {
        currentTime = 0;
        index = 0;
        indexMusic = 0;
    }

    void Update()
    {
        if (isPlaying)
        {
            PlayCurrentNoteOnMusic(currentMusic.notes[index]);
            CheckIfPlayingCorrectly(currentMusic.notes[index]);
            GoToCorrectPosition();
        }
    }
    public void GoToCorrectPosition() {
        Vector2 finalpos = new Vector2(currentMusic.initialPos - distanceBetweenNotes, partitureImage.rectTransform.localPosition.y);
        if (distanceBetweenNotes < previousDistanceBetweenNotes)
            partitureImage.rectTransform.localPosition = finalpos;
        else
        {
            partitureImage.rectTransform.localPosition = Vector2.MoveTowards(partitureImage.rectTransform.localPosition, finalpos, 0.02f * speed);
            previousDistanceBetweenNotes = distanceBetweenNotes;
        }
    }
    public void NextMusic() {
        //swapMode.ResetSwapMode();
        StopMusic();
        indexMusic++;
        if (indexMusic >= repertoire.Count) {
            indexMusic = 0;
        }
        currentMusic = repertoire[indexMusic];
        PlayMusic();
    }
    public void PlayCurrentNoteOnMusic(PlaybackNote current)
    {
        dev.text = currentMusic.notes[index].Note.ToString()+ (currentMusic.notes[index].Octave).ToString();
        if (flag)
            flute.PressHoles(current.Note, current.Octave -1);
        if ((audioSource.time >= currentTime))
        {//mudar nota
            if (index + 1 >= currentMusic.notes.Length)
            {//musica acabou
                StopMusic();
                return;
            }
            index++;
            if (modo == Modos.lento) {
                currentTime += currentMusic.notes[index].Time * currentMusic.bpm;
            }
            else {
                currentTime += currentMusic.notes[index].Time;
            }
            if (index + 7 < currentMusic.notes.Length)
            {
                ReorderSpriteNotes(currentMusic.notes[index + 7]);
            }
            else
            {
                ReorderSpriteNotes(voidNote);
            }
            flag = true;
        }
    }
    public void CheckIfPlayingCorrectly(PlaybackNote current)
    {
        if (current.Note.ToString() == noteText.text) //verificar oitava tb
        {
            particles.startColor = Color.green;
        }
        else
        {
            particles.startColor = Color.red;
        }
    }
    public void ReorderSpriteNotes(PlaybackNote currentNote)
    {
        distanceBetweenNotes = currentMusic.notes[index-1].Passo/5;

        //partitureImage.rectTransform.localPosition = new Vector2(currentMusic.initialPos - distanceBetweenNotes, partitureImage.rectTransform.localPosition.y);
    }
    public void OrderSpriteNotes()
    {
        partitureImage.rectTransform.localPosition = Vector3.zero;
        partitureImage.sprite = currentMusic.partiture;
        //partitureImage.rectTransform.sizeDelta = new Vector2(currentMusic.tamanhoNota * currentMusic.notes.Length + 3, 48);
        partitureImage.rectTransform.sizeDelta = new Vector2(currentMusic.Width, 240);
        partitureImage.rectTransform.localPosition = new Vector2(currentMusic.initialPos, partitureImage.rectTransform.localPosition.y);
        distanceBetweenNotes = currentMusic.notes[0].Passo / 5;
    }
    public void PlayMusic()
    {
        if (isPaused) {
            isPlaying = true;
            audioSource.UnPause();
            isPaused = false;
            return;
        }
        musicTitle.text = currentMusic.MusicName;
        isPaused = false;
        OrderSpriteNotes();
        //audioSource.clip = !swapMode.acompanhamento ? currentMusic.audioClip : currentMusic.acompanhamento;
        audioSource.clip = swapMode.ReturnAudioClip();
        audioSource.Play();
        currentTime = currentMusic.notes[0].Time;
        SetaToInitialPosition();
        flag = true;
        index = 0;
        particlesGO.SetActive(true);
        //setaGO.SetActive(true);
    }
    public void SetaToInitialPosition() {
        isPlaying = true;
        partitureImage.rectTransform.localPosition = new Vector2(currentMusic.initialPos, partitureImage.rectTransform.localPosition.y);
    }
    public void StopMusic()
    {
        notasPosicoes.SetActive(false);
        isPlaying = false;
        isPaused = false;
        particlesGO.SetActive(false);
        //setaGO.SetActive(false);
        audioSource.Stop();
    }
    public void PauseMusic() {
        if (isPaused)
            PlayMusic();
        else
        {
            audioSource.Pause();
            isPlaying = false;
            isPaused = true;
        }
    }
}
