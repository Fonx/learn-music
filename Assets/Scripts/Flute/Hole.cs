﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
    public HighlightForFlute highlight;
    public MeshRenderer rend;
    public enum Status
    {
        Opened, HalfPressed, Pressed
    };
    public Status status;

    void Start()
    {
        rend.enabled = false;
    }
    public virtual void ChangeHoleStatus(Status status) {
        this.status = status;
        if (status == Status.Pressed) {
            rend.enabled = true;
            highlight.outlineMode = Mode.GreenAll;
            highlight.UpdateOutlineMode();
            return;
        }
        else if (status == Status.HalfPressed) {
            Debug.Log(gameObject.name);
            rend.enabled = true;
            highlight.outlineMode = Mode.YellowAll;
            highlight.UpdateOutlineMode();
            return;
        }
        else {
            rend.enabled = false;
            highlight.outlineMode = Mode.None;
            highlight.UpdateOutlineMode();
            return;
        }
    }
}
