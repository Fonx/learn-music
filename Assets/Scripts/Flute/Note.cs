﻿using UnityEngine;

public class Note : MonoBehaviour
{
    [SerializeField]
    [Range(0, 2)]
    private int octave;
    public int Octave
    {
        get
        {
            return octave;
        }
    }
    [SerializeField] private Notes note;
    public Notes CurrentNote
    {
        get
        {
            return note;
        }
    }
    [SerializeField] private AudioClip initialAudioClip; //Audio Clip inicial, onde o sopro é ruim.
    public AudioClip InitialAudioClip
    {
        get
        {
            return initialAudioClip;
        }
    }

    [SerializeField] private AudioClip finalAudioClip; //Final Audio Clip, onde o sopro é o ideal e ficará em loop tocando
    public AudioClip FinalAudioClip
    {
        get
        {
            return finalAudioClip;
        }
    }

    [SerializeField] private Sprite noteSprite; //Partitura da nota
    public Sprite NoteSprite
    {
        get
        {
            return noteSprite;
        }
    }

    //Apenas informativo
    //s = Sustenido
    public enum Notes { C, Cs, D, Ds, E, F, Fs, G, Gs, A, As, B, Void,Em }
    //Void Note é uma nota vazia, apenas para dar um timing sem som entre notas ;~;
}
