﻿using UnityEngine;

/// <summary>
/// Classe utilizada para representar as bolinhas de Aberto/Apertado/Meia apertada na flauta.
/// </summary>
public class UIHole : MonoBehaviour
{
    private Hole.Status currentState;
    [SerializeField] private GameObject pressed;
    [SerializeField] private GameObject halfPressed;

    public Hole.Status State
    {
        set
        {
            currentState = value;

            if (currentState == Hole.Status.Pressed)
            {
                pressed.SetActive(true);
                halfPressed.SetActive(false);
            }
            else if (currentState == Hole.Status.HalfPressed)
            {
                pressed.SetActive(false);
                halfPressed.SetActive(true);
            }
            else
            {
                DeactivateAll();
            }
        }
    }

    private void Awake()
    {
        DeactivateAll();
        currentState = Hole.Status.Opened;
    }

    private void DeactivateAll()
    {
        pressed.SetActive(false);
        halfPressed.SetActive(false);
    }
}
