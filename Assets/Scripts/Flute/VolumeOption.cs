﻿using System.Globalization;
using System.Collections.Generic;
//using UnityEditor;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class VolumeOption : MonoBehaviour
{
    [SerializeField] private Slider volumeSlider;
    [SerializeField] private TextMeshProUGUI percentText;
    public AudioSource audioSource;
    public Flute flute;
    private void Awake()
    {
        //audioSources = GetAllObjectsOnlyInScene().ToArray();
        volumeSlider.maxValue = 1f;
        volumeSlider.minValue = 0f;
        volumeSlider.value = 0.71f; //Entre 0 e 1 pls ;~;
        percentText.text = ((int)(volumeSlider.value * 100f)).ToString();

        audioSource.volume = 0.71f;
        flute.currentVolume = 0.71f;
        //for (int i = 0; i < audioSources.Length; i++)
        //{
        //    audioSources[i].volume = 0.71f;
        //}
    }

    //Perhaps caquita
    public void OnValueChanged()
    {
        //for (int i = 0; i < audioSources.Length; i++)
        //{
        //    audioSources[i].volume = volumeSlider.value;
        //}
        audioSource.volume = volumeSlider.value;
        flute.currentVolume = volumeSlider.value;
        percentText.text = ((int)(volumeSlider.value * 100f)).ToString();
    }

    //private List<AudioSource> GetAllObjectsOnlyInScene()
    //{
    //    List<AudioSource> objectsInScene = new List<AudioSource>();

    //    foreach (AudioSource go in Resources.FindObjectsOfTypeAll(typeof(AudioSource)) as AudioSource[])
    //    {

    //        //if (!EditorUtility.IsPersistent(go.transform.root.gameObject) && !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
    //        //{
    //        //    objectsInScene.Add(go);
    //        //}
    //    }

    //    return objectsInScene;
    //}
}
