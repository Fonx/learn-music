﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectSetActiveDinamic : MonoBehaviour
{
    public void ChangeActiveState()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
