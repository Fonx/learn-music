﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetTransformOnEnable : MonoBehaviour
{
    private void OnEnable()
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(Vector3.zero);
    }
}
