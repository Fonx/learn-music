﻿namespace Granulometria
{
    public interface IRecipient
    {
        void AddStorable(IStorable storable);
        void RemoveStorable(IStorable storable);
        bool IsClosed { get; set; }
    }

    public interface IStorable
    {
        void SetRecipient(IRecipient recipient);
    }
}