using UnityEngine;
using UnityEngine.UI;
public class Notification : MonoBehaviour
{
    class SingletonCreator
    {
        static SingletonCreator() { }
        internal static Notification instance;
    }
    public static Notification UniqueInstance
    {
        get
        {
            return SingletonCreator.instance;
        }
    }
    [Header("OBJECT")]
    [SerializeField] private GameObject notificationObject;
    [SerializeField] private Animator notificationAnimator;
    [SerializeField] private Text content;
    [SerializeField] private Image imageSource;

    [Header("VARIABLES")]
    [SerializeField] private string animationNameIn;
    [SerializeField] private string animationNameOut;

    [Header("COLOR")]
    [SerializeField] private Color infoColor; //5F6973
    [SerializeField] private Color warningColor; //F0D848 Mostarda
    [SerializeField] private Color errorColor; //F23A3A
    Notification() { }

    private void Awake()
    {
        SingletonCreator.instance = this;
        imageSource.color = infoColor;
        notificationObject.SetActive(false);
    }
    public void Warning(string message)
    {
#if UNITY_EDITOR
        Debug.Log(message);
#endif

        imageSource.color = warningColor;
        notificationObject.SetActive(true);
        content.text = message;
        notificationAnimator.Play(animationNameIn);
        notificationAnimator.Play(animationNameOut);
    }
    public void Error(string message)
    {
#if UNITY_EDITOR
        Debug.Log(message);
#endif

        imageSource.color = errorColor;
        notificationObject.SetActive(true);
        content.text = message;
        notificationAnimator.Play(animationNameIn);
        notificationAnimator.Play(animationNameOut);
    }
    public void Info(string message)
    {
#if UNITY_EDITOR
        Debug.Log(message);
#endif

        imageSource.color = infoColor;
        notificationObject.SetActive(true);
        content.text = message;
        notificationAnimator.Play(animationNameIn);
        notificationAnimator.Play(animationNameOut);
    }
}