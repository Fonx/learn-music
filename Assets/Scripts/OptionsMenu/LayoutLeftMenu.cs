﻿using UnityEngine;
using UnityEngine.UI;

public class LayoutLeftMenu : MonoBehaviour
{
    public VerticalLayoutGroup verticalLayout;
    public RectTransform listaCameras;
    public RectTransform leftPanel;
    public float height;

    void OnGUI()
    {
        if (height != listaCameras.sizeDelta.y)
        {
            height = listaCameras.localScale.y;
            verticalLayout.spacing = height * 170 + 10;
            LayoutRebuilder.ForceRebuildLayoutImmediate(leftPanel);
        }
        
    }
}
