﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class OptionsPanel : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    private bool isVisible;
    private void Start()
    {
        canvasGroup = gameObject.GetComponent<CanvasGroup>();
    }
    public void ToggleVisibility()
    {
        canvasGroup.alpha = isVisible ? 0 : 1;
        canvasGroup.blocksRaycasts = !isVisible;
        Time.timeScale = isVisible ? 1 : 0;
        isVisible = !isVisible;
    }
    public void Restart()
    {
        //ToggleVisibility();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
    }
    public void Quit()
    {
        Application.Quit();
    }
}
