﻿using UnityEngine;
using UnityEngine.UI;
public class Version : MonoBehaviour
{
    void Start()
    {
        gameObject.GetComponent<Text>().text = "v " + Application.version;
    }
}
