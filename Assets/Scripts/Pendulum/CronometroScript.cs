﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using UnityEngine.UI;
using UnityEditor;

public class CronometroScript : MonoBehaviour
{

    [SerializeField] private Text scaleTitle;
    //private Stopwatch stopwatch = new Stopwatch();
    private bool isRunning;
    private float elapseTime;   //tempo que será mostrado pelo cronometro
    private int seconds;
    private int minutes;
    private int hours;
    private string secondsText;
    private string minutesText;
    private string hoursText;
    public float timeFactor;    //fator de tempo usado para acelerar ou retardar o tempo (manipulação do tempo)
    //public Text startText;      //texto do botão start/stop
    public Text stopwatchText;  //texto mostrado pelo cronometro
    public Text daysText;
    //private float elapseTime_corrigido; //tempo real multiplicado pelo fator de tempo 
    private float elapseTime_prev;  //tempo real que passou do instante anterior em segundos
    private float elapseTime_real;  //tempo real que passou em segundos
    private float deltaT;   //diferença entre o tempo do instante atual e anterior considerando manipulação do tempo
    private float elapseTime_SomaAnterior; //tempo que foi mostrado pelo cronometro no loop anterior
    private int days;
    private bool userclick;

    public float TimeScale = 1f;

    public float ElapseTime
    {
        get
        {
            return elapseTime;
        }
    }

    public bool IsRunning
    {
        get
        {
            return isRunning;
        }
    }

    // Use this for initialization
    void Start()
    {
        //gameObject.transform.Find("TimeText").GetComponent<Text>(); VINIBOSSCODE
        elapseTime = 0;
        timeFactor = 1;
        //startText.text = "Start";
        stopwatchText.text = "0000.00";
        //elapseTime_corrigido = 0; 
        elapseTime_prev = 0;
        elapseTime_real = 0;
        elapseTime_SomaAnterior = 0;
        deltaT = 0;
        days = 0;
        daysText.text = "00";
        userclick = false;

        scaleTitle.text = "Escala de tempo : " + TimeScale.ToString("0.#") + "x";
    }

    public void StartCount()
    {
        if (isRunning)
        {
            isRunning = false;
            //startText.text = "Start";
        }
        else
        {
            isRunning = true;
            //startText.text = "Pause";
        }

    }

    //Escala de tempo : 1.0x
    public void OnValueChanged(float value)
    {
        TimeScale = 1f * value;
        //timeFactor = Time.timeScale;
        scaleTitle.text = "Escala de tempo : " + TimeScale.ToString("0.#") + "x";
    }

    public void ResetCount(bool data)
    {
        //UnityEngine.Debug.Log("Time elapsed: 00:00:00s");

        elapseTime = 0;

        stopwatchText.text = "000.00";
        elapseTime_prev = 0;
        elapseTime_real = 0;
        elapseTime_SomaAnterior = 0;
        deltaT = 0;
        elapseTime = 0;
        minutes = 0;
        seconds = 0;
        if (data)
        {
            daysText.text = "00";
            days = 0;
        }

    }

    public void WriteHour(float value)
    {
        stopwatchText.text = value.ToString("00") + "h";
    }


    void FixedUpdate()
    {
        if (isRunning)
        {
            elapseTime += (float)(Time.fixedDeltaTime * TimeScale);

            if (elapseTime > 9999.99f)
            {
                stopwatchText.text = (elapseTime.ToString("999.99"));
            }
            else
            {
                stopwatchText.text = (elapseTime.ToString("000.00"));
            }
            //daysText.text = days.ToString("00");
            //UnityEngine.Debug.Log("Time elapsed: " + hoursText +":"+ minutesText +":"+ secondsText + "s");
            //UnityEngine.Debug.Log(stopwatchText.text);
        }
    }


}
