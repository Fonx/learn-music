﻿using UnityEngine;

public class DragWindow : MonoBehaviour
{
    private float offsetX, offsetY;
    private Vector3 initialPos;
    public Transform window;
    private void Start()
    {
        initialPos = window.position;
    }
    public void BeginDrag()
    {
        offsetX = window.position.x - Input.mousePosition.x;
        offsetY = window.position.y - Input.mousePosition.y;
    }
    public void OnDrag()
    {
        window.position = new Vector3(Input.mousePosition.x + offsetX, Input.mousePosition.y + offsetY, 0);
    }
    public void Restart()
    {
        window.position = initialPos;
    }
}
