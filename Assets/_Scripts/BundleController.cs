﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BundleController : MonoBehaviour
{
    public BundleManager bundle;

    public AssetBundleData bundledata;
    public AudioClip[] downloadedAudios;

    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
            Debug.Log("baixando audios");
            DownloadAssetBundle();
        }
        if (Input.GetKeyDown(KeyCode.A)) {
            downloadedAudios = bundledata.assetBundle.LoadAllAssets<AudioClip>();
        }
    }

    public void DownloadAssetBundle() {
        bundle.DownloadAssetBundle(bundledata);
        
    }
}
