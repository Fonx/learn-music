﻿using Dream;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ControladorBitp3 : MonoBehaviour   //arrumar qnd tiver mais de uma musica
{
    public TMP_Dropdown melodia;
    public TMP_Dropdown modo;

    public List<Music> musics;

    public AudioClip currentMusic;

    public Music currentMelodia;
    public GameRulesP3 gamerules;

    public AudioClip ReturnAudioClip()
    {
        melodia.interactable = modo.value == 0 ? false : true;
        currentMelodia = musics[melodia.value];
        if (modo.value == 0)
        {
            gamerules.modo = Modos.ouvir;
            return currentMelodia.solo;
        }
        else if (modo.value == 1)
        {
            gamerules.modo = Modos.normal;
            return currentMelodia.audioClip;
        }
        else
        {
            gamerules.modo = Modos.lento;
            return currentMelodia.audioClipLento;
        }
    }

    public Music ReturnOtherMusic() {
        if (melodia.value ==0) {
            return musics[1];
        }
        else {
            return musics[0];
        }
    }
}
