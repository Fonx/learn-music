﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluteHighlightController : MonoBehaviour
{
    [SerializeField] private List<HighlightForFlute> highlights;

    public void HighLightGreen() {
        for (int i = 0; i < highlights.Count; i++) {
            highlights[i].outlineMode = Mode.GreenAll;
        }
    }
    public void HightLightRed() {
        for (int i = 0; i < highlights.Count; i++)
        {
            highlights[i].outlineMode = Mode.RedAll;
        }
    }
    public void HighDont() {
        for (int i = 0; i < highlights.Count; i++)
        {
            highlights[i].outlineMode = Mode.None;
        }
    }
}
