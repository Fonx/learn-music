﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Globalization;
using System;

public class GameRules : MonoBehaviour
{
    public Sprite primeiraOitavaSprite;
    public Sprite segundaOitavaSprite;
    public Image imageReferenceNotasOitava;
    public TMP_Dropdown dropDownOitava;
    public int oitava = 0;
    public Flute flute;
    private int index;
    public List<Transform> setaPosicoes;
    public List<Transform> setaPosicoes2;
    public GameObject seta;
    public List<Note.Notes> enumNotes;
    public AudioSource audioSource;

    public TMP_Text noteText;
    public TMP_Text frequenciText;

    public float minFreq;
    public float maxFreq;
    public TMP_InputField minFreqInput;
    public TMP_InputField maxFreqInput;
    public ParticleSystem particles;
    public GameObject particlesGO;
    private int amount;
    private int totalAmount;
    void Start()
    {
        totalAmount = 150;
        particlesGO = particles.gameObject;
        Application.targetFrameRate = 60;
        minFreq = 0;
        maxFreq = 9999f;
        index = 0;
        seta.transform.position = setaPosicoes[index].position;
    }
    private void Update()
    {
        //if (noteText.text == "-")
        //{
        //    particlesGO.SetActive(false);
        //    return;
        //}
        //else {
        //    particlesGO.SetActive(true);
        //}

        //if (flute.isPlaying) {

        //if (actualFrequency) {
        //    return;
        //}
        try {
            float actualFrequency = float.Parse(frequenciText.text, CultureInfo.InvariantCulture);
            if ((enumNotes[index].ToString() == noteText.text) &&
                (actualFrequency > minFreq && actualFrequency < maxFreq))
            {
                particles.startColor = Color.green;
                amount++;
                if (amount >= totalAmount)
                    Notification.UniqueInstance.Info("Você conseguiu reproduzir a nota " + noteText.text + " com êxito.");
            }
            else
            {
                amount = 0;
                particles.startColor = Color.red;
            }
        }
        catch (Exception e) {
            particles.startColor = Color.red;
        }
       // }
    }
    public void ChangeParticleColor(){
        Debug.Log("entrou");
        particles.startColor = Color.green;
    }
    public void Play() {
        if (oitava == 0)
        {
            if (index >= 7)
            {
                flute.Play(enumNotes[index], 1);
            }
            else
                flute.Play(enumNotes[index], 0);
        }
        else {
            if (index == 7)
            {
                flute.Play(enumNotes[0], 2);
            }
            else if (index == 8) 
            {
                flute.Play(enumNotes[1], 2);
            }
            else
                flute.Play(enumNotes[index], 1);
        }
        particlesGO.SetActive(true);
    }
    public void Next() {
        index++;
        if (index > 7)
            index = 0;
        seta.transform.position = oitava == 0 ? setaPosicoes[index].position : setaPosicoes2[index].position;
        //if (oitava == 0)
        //{
        //    if (index > 7)
        //        index = 0;
        //    seta.transform.position = setaPosicoes[index].position;
        //}
        //else {
        //    if (index > 8)
        //        index = 0;
        //    seta.transform.position = setaPosicoes2[index].position;
        //}
        //flute.Play(enumNotes[index], 0);
        Play();
        totalAmount = index >= 4 ? 115 : 150;
    }
    public void Previous() {
        index--;
        if (oitava == 0)
        {
            if (index < 0)
                index = 7;
            seta.transform.position = setaPosicoes[index].position;
        }
        else {
            if (index < 0)
                index = 8;
            seta.transform.position = setaPosicoes2[index].position;
        }
        //flute.Play(enumNotes[index], 0);
        Play();
        totalAmount = index >= 4 ? 115 : 150;
    }
    public void Pause() {
        particlesGO.SetActive(true);
        audioSource.Stop();
        flute.isPlaying = false;
        //flute.particles.SetActive(false);
    }

    public void ChangeGameFreq() {
        minFreq = float.Parse(minFreqInput.text);
        maxFreq = float.Parse(maxFreqInput.text);
        Notification.UniqueInstance.Info("Novos valores de frequência min/max atualizados.");
    }

    public void ChangeOitava() {
        Pause();
        oitava = dropDownOitava.value == 0 ? 0 : 1;
        imageReferenceNotasOitava.sprite = oitava == 0 ? primeiraOitavaSprite : segundaOitavaSprite;

        if (oitava == 0)
        {
            index = 0;
            seta.transform.position = setaPosicoes[index].position;
        }
        else {
            index = 0;
            seta.transform.position = setaPosicoes2[index].position;
        }
    }
}
