﻿using Dream;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameRulesMusica : MonoBehaviour
{
    public GameObject setaGO;
    public PlaybackNote voidNote;
    public GameObject notasPosicoes;
    public ParticleSystem particles;
    public GameObject particlesGO;
    private bool flag; //so executar pressHoles 1 vez.
    public Flute flute;
    public Music currentMusic;
    public List<Music> repertoire;
    public int index;
    public AudioSource audioSource;
    public float currentTime;
    private bool isPlaying;
    public TMP_Text noteText;
    public SpriteController spriteController;
    void Start()
    {
        currentTime = 0;
        index = 0;
    }

    void Update()
    {
        if (isPlaying) {
            PlayCurrentNoteOnMusic(currentMusic.notes[index]);
            CheckIfPlayingCorrectly(currentMusic.notes[index]);
        }
    }

    public void PlayCurrentNoteOnMusic(PlaybackNote current) {
        if(flag)
            flute.PressHoles(current.Note, current.Octave);
        if ((audioSource.time >= currentTime))
        {//mudar nota
            if (index +1 >= currentMusic.notes.Length)
            {//musica acabou
                StopMusic();
                return;
            }
            index++;
            currentTime += currentMusic.notes[index].Time;
            if (index + 7 < currentMusic.notes.Length)
            {
                ReorderSpriteNotes(currentMusic.notes[index + 7]);
            }
            else
            {
                ReorderSpriteNotes(voidNote);
            }
            flag = true;
        }
    }
    public void CheckIfPlayingCorrectly(PlaybackNote current) {
        if (current.Note.ToString() == noteText.text) //verificar oitava tb
        {
            particles.startColor = Color.green;
        }
        else
        {
            particles.startColor = Color.red;
        }
    }
    public void ReorderSpriteNotes(PlaybackNote currentNote) {
        spriteController.ReorderSpriteNotes(currentNote);
    }
    public void OrderSpriteNotes() {
        spriteController.OrderSpriteNotes(currentMusic.notes);
    }
    public void PlayMusic() {
        notasPosicoes.SetActive(true);
        audioSource.clip = currentMusic.audioClip;
        audioSource.Play();
        currentTime = currentMusic.notes[0].Time;
        isPlaying = true;
        flag = true;
        index = 0;
        OrderSpriteNotes();
        particlesGO.SetActive(true);
        setaGO.SetActive(true);
    }
    public void StopMusic() {
        Debug.Log("entrou");
        notasPosicoes.SetActive(false);
        isPlaying = false;
        particlesGO.SetActive(false);
        setaGO.SetActive(false);
    }
}
