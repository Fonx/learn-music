﻿using Dream;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameRulesP3 : MonoBehaviour
{
    public Modos modo = Modos.normal;
    public float speed = 120f;
    private float distanceBetweenNotes;
    private float distanceBetweenNotes2;
    public TMP_Text musicTitle;
    public Image partitureImage;
    public Image partitureImage2;
    public float currentTime;
    public float currentTime2;
    public ControladorBitp3 swapMode;
    //public GameObject setaGO;
    public PlaybackNote voidNote;
    public GameObject notasPosicoes;
    public ParticleSystem particles;
    public GameObject particlesGO;
    private bool flag; //so executar pressHoles 1 vez.
    public Flute flute;
    public Music currentMusic;
    public Music currentMusic2;
    //public List<Music> repertoire;
    public int index;
    public int index2;
    public int indexMusic;
    public AudioSource audioSource;

    private bool isPlaying;
    private bool isPaused;
    public TMP_Text noteText;

    void Start()
    {
        currentTime = 0;
        index = 0;
        index2 = 0;
        indexMusic = 0;
    }

    void Update()
    {
        if (isPlaying)
        {
            PlayCurrentNoteOnMusic(currentMusic.notes[index]);
            PlayCurrentNoteOnOtherFlute(currentMusic2.notes[index2]);
            CheckIfPlayingCorrectly(currentMusic.notes[index]);
            GoToCorrectPosition();
        }
    }
    public void GoToCorrectPosition()
    {
        Vector2 finalpos = new Vector2(currentMusic.initialPos - distanceBetweenNotes, partitureImage.rectTransform.localPosition.y);
        partitureImage.rectTransform.localPosition = Vector2.MoveTowards(partitureImage.rectTransform.localPosition, finalpos, 0.01f * speed);

        Vector2 otherFinalPos = new Vector2(currentMusic2.initialPos - distanceBetweenNotes2, partitureImage2.rectTransform.localPosition.y);
        partitureImage2.rectTransform.localPosition = Vector2.MoveTowards(partitureImage2.rectTransform.localPosition, otherFinalPos, 0.01f * speed);
    }
    //public void NextMusic()
    //{
    //    swapMode.ResetSwapMode();
    //    StopMusic();
    //    indexMusic++;
    //    if (indexMusic >= repertoire.Count)
    //    {
    //        indexMusic = 0;
    //    }
    //    currentMusic = repertoire[indexMusic];
    //    PlayMusic();
    //}
    public void PlayCurrentNoteOnMusic(PlaybackNote current)
    {
        if (flag)
            flute.PressHoles(current.Note, current.Octave - 1);
        if ((audioSource.time >= currentTime))
        {//mudar nota
            if (index + 1 >= currentMusic.notes.Length)
            {//musica acabou
                if (index2 + 1 >= currentMusic2.notes.Length)
                {//musica acabou
                 StopMusic();
                    return;
                }
                //StopMusic();
                return;
            }
            index++;
            if (modo == Modos.lento) {
                currentTime += currentMusic.notes[index].Time * currentMusic.bpm;
            }
            else {
                currentTime += currentMusic.notes[index].Time;
            }
            ReorderSpriteNotes();
            flag = true;
        }
    }

    public void PlayCurrentNoteOnOtherFlute(PlaybackNote current) {
        if ((audioSource.time >= currentTime2))
        {//mudar nota
            if (index2 + 1 >= currentMusic2.notes.Length)
            {//musica acabou
                //StopMusic();
                return;
            }
            index2++;
            if (modo == Modos.lento)
            {
                currentTime2 += currentMusic2.notes[index2].Time * currentMusic2.bpm;
            }
            else
            {
                currentTime2 += currentMusic2.notes[index2].Time;
            }
            ReorderSpriteNotesOther();
            flag = true;
        }
    }


    public void CheckIfPlayingCorrectly(PlaybackNote current)
    {
        if (current.Note.ToString() == noteText.text) //verificar oitava tb
        {
            particles.startColor = Color.green;
        }
        else
        {
            particles.startColor = Color.red;
        }
    }
    public void ReorderSpriteNotes()
    {
        distanceBetweenNotes = currentMusic.notes[index - 1].Passo / 5;
        //partitureImage.rectTransform.localPosition = new Vector2(currentMusic.initialPos - distanceBetweenNotes, partitureImage.rectTransform.localPosition.y);
    }
    public void ReorderSpriteNotesOther() {
        distanceBetweenNotes2 =  currentMusic2.notes[index2 - 1].Passo / 5;
    }
    public void OrderSpriteNotes()
    {
        partitureImage.rectTransform.localPosition = Vector3.zero;
        partitureImage.sprite = currentMusic.partiture;
        //partitureImage.rectTransform.sizeDelta = new Vector2(currentMusic.tamanhoNota * currentMusic.notes.Length + 3, 48);
        partitureImage.rectTransform.sizeDelta = new Vector2(currentMusic.Width, 240);
        partitureImage.rectTransform.localPosition = new Vector2(currentMusic.initialPos, partitureImage.rectTransform.localPosition.y);
        distanceBetweenNotes = currentMusic.notes[0].Passo / 5;
    }

    public void OrderSpriteNotesOther() {
        partitureImage2.rectTransform.localPosition = Vector3.zero;
        partitureImage2.sprite = currentMusic2.partiture;
        //partitureImage.rectTransform.sizeDelta = new Vector2(currentMusic.tamanhoNota * currentMusic.notes.Length + 3, 48);
        partitureImage2.rectTransform.sizeDelta = new Vector2(currentMusic2.Width, 240);
        partitureImage2.rectTransform.localPosition = new Vector2(currentMusic2.initialPos, partitureImage2.rectTransform.localPosition.y);
        distanceBetweenNotes2 = currentMusic2.notes[0].Passo / 5;
    }

    public void PlayMusic()
    {
        if (isPaused)
        {
            isPlaying = true;
            audioSource.UnPause();
            isPaused = false;
            return;
        }
        audioSource.clip = swapMode.ReturnAudioClip();
        currentMusic = swapMode.currentMelodia;
        currentMusic2 = swapMode.ReturnOtherMusic();
        musicTitle.text = currentMusic.MusicName;
        isPaused = false;
        OrderSpriteNotes();
        OrderSpriteNotesOther();
        //audioSource.clip = !swapMode.acompanhamento ? currentMusic.audioClip : currentMusic.acompanhamento;
        audioSource.Play();
        currentTime = currentMusic.notes[0].Time;
        currentTime2 = currentMusic2.notes[0].Time;

        flag = true;
        index = 0;
        index2 = 0;
        particlesGO.SetActive(true);
        SetaToInitialPosition();
        //setaGO.SetActive(true);
    }
    public void SetaToInitialPosition()
    {
        isPlaying = true;
        partitureImage.rectTransform.localPosition = new Vector2(currentMusic.initialPos, partitureImage.rectTransform.localPosition.y);
        partitureImage2.rectTransform.localPosition = new Vector2(currentMusic2.initialPos,partitureImage2.rectTransform.localPosition.y);
    }
    public void StopMusic()
    {
        notasPosicoes.SetActive(false);
        isPlaying = false;
        isPaused = false;
        particlesGO.SetActive(false);
        //setaGO.SetActive(false);
        audioSource.Stop();
    }
    public void PauseMusic()
    {
        if (isPaused)
            PlayMusic();
        else
        {
            audioSource.Pause();
            isPlaying = false;
            isPaused = true;
        }
    }
}
public enum Modos {normal,lento,ouvir};
