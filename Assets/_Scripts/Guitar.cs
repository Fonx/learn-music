﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Guitar : MonoBehaviour
{
    //private Corda corda;
    private Animator animator;
    public Image tablatura;
    public Sprite Asprite;
    public Sprite Dsprite;
    public Sprite Esprite;
    public Sprite Bsprite;
    public Sprite Emsprite;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void PlayNote(Note.Notes note, int octave)
    {
        Debug.Log(note);
        if (note == Note.Notes.D) {
            animator.SetTrigger("D");
            tablatura.sprite = Dsprite;
        }
        else if (note == Note.Notes.A) {
            animator.SetTrigger("A7");
            tablatura.sprite = Asprite;
        }
        else if (note == Note.Notes.E) {
            animator.SetTrigger("E");
            tablatura.sprite = Esprite;
        }
        else if (note == Note.Notes.B) {
            animator.SetTrigger("B7");
            tablatura.sprite = Bsprite;
        }
        else if (note == Note.Notes.Em) {
            animator.SetTrigger("Em");
            tablatura.sprite = Emsprite;
        }
    }
}
public enum Corda { };