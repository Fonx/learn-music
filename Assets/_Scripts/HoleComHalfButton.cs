﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleComHalfButton : Hole
{
    public GameObject halfhole;

    public void Start()
    {
        rend.enabled = false;
        halfhole.SetActive(true);
    }
    public override void ChangeHoleStatus(Status status)
    {
        this.status = status;
        if (status == Status.Pressed)
        {
            halfhole.SetActive(false);
            rend.enabled = true;
            highlight.outlineMode = Mode.GreenAll;
            highlight.UpdateOutlineMode();
            return;
        }
        else if (status == Status.HalfPressed)
        {
            halfhole.SetActive(true);
            rend.enabled = false;
            highlight.outlineMode = Mode.YellowAll;
            highlight.UpdateOutlineMode();
            return;
        }
        else
        {
            halfhole.SetActive(false);
            rend.enabled = false;
            highlight.outlineMode = Mode.None;
            highlight.UpdateOutlineMode();
            return;
        }
    }
}
