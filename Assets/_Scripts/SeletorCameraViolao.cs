﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeletorCameraViolao : MonoBehaviour
{
    public GameObject cameraNormal;
    public GameObject cameraZoom;


    public GameObject cameraNormalcanvas;
    public GameObject cameraZoomcanvas;

    public bool zoom = true;

    public void ChangeCamera()
    {
        if (!zoom)
        {
            cameraNormal.SetActive(false);
            cameraZoom.SetActive(true);

            cameraNormalcanvas.SetActive(false);
            cameraZoomcanvas.SetActive(true);
            zoom = true;
        }
        else
        {
            cameraNormal.SetActive(true);
            cameraZoom.SetActive(false);

            cameraNormalcanvas.SetActive(true);
            cameraZoomcanvas.SetActive(false);
            zoom = false;
        }
    }
}
