﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeletorDeCamera : MonoBehaviour
{
    public GameObject cameraLateral1;
    public GameObject cameraLateral2;
    public GameObject cameraVertical1;
    public GameObject cameraVertical2;

    public GameObject cameraLateral1canvas;
    public GameObject cameraLateral2canvas;
    public GameObject cameraVertical1canvas;
    public GameObject cameraVertical2canvas;

    public bool vertical = false;

    public void ChangeCamera() {
        if (!vertical)
        {
            cameraLateral1.SetActive(false);
            cameraLateral2.SetActive(false);
            cameraVertical1.SetActive(true);
            cameraVertical2.SetActive(true);
            cameraLateral1canvas.SetActive(false);
            cameraLateral2canvas.SetActive(false);
            cameraVertical1canvas.SetActive(true);
            cameraVertical2canvas.SetActive(true);
            vertical = true;
        }
        else {
            cameraLateral1.SetActive(true);
            cameraLateral2.SetActive(true);
            cameraVertical1.SetActive(false);
            cameraVertical2.SetActive(false);
            cameraLateral1canvas.SetActive(true);
            cameraLateral2canvas.SetActive(true);
            cameraVertical1canvas.SetActive(false);
            cameraVertical2canvas.SetActive(false);
            vertical = false;
        }
    }

}
