﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SeletorDeFlauta : MonoBehaviour
{
    public TMP_Dropdown dropDown;
    public TMP_Dropdown dropDown2;
    public Flute flute;
    public Transform buracoFrente;
    public Transform buracoTras;

    public void ChangeFluteMaterial() {
        flute.ChangeFluteMaterial(flute.fluteMaterials[dropDown.value]);
        flute.selectedFluteIndex = dropDown.value;
    }
    public void ChangeFluteType() {
        flute.IsGermanic = dropDown2.value == 0 ? false : true;
        ChangeFluteHoleSize();
    }

    public void Start()
    {
        ChangeFluteType();
        dropDown.value = flute.selectedFluteIndex;
        ChangeFluteHoleSize();
    }

    public void ChangeFluteHoleSize() {
        buracoTras.localScale = !flute.IsGermanic ? Vector3.one * 0.01f : Vector3.one * 0.007f;
        buracoFrente.localScale = !flute.IsGermanic ? Vector3.one * 0.007f : Vector3.one * 0.01f;
    }
}
