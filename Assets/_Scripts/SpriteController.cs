﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteController : MonoBehaviour
{
    public List<Image> images;
    public Flute flute;


    public void OrderSpriteNotes(PlaybackNote[] notes)
    {
        for (int i = 0; i < 8; i++) {
            images[i].sprite = flute.GetSprite(notes[i].Note, notes[i].Octave);
        }
    }
    public void ReorderSpriteNotes(PlaybackNote newNote)
    {
        int i = 0;
        for (i = 0; i < 7; i++)
        {
            images[i].sprite = images[i + 1].sprite;
        }
        images[i].sprite = flute.GetSprite(newNote.Note, newNote.Octave);
    }
}
