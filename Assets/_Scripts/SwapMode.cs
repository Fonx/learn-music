﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class SwapMode : MonoBehaviour
{
    public GameRulesRolling rules;
    public TMP_Dropdown dropDown;
    public AudioSource audioSource;

    public void Start()
    {

    }
    public void ChangeGameMode() {
        audioSource.Stop();
        rules.PlayMusic();
    }
    public AudioClip ReturnAudioClip() {
        switch (dropDown.value)
        {
            case 0:
                rules.modo = Modos.normal;
                return rules.currentMusic.audioClip;
            case 1:
                rules.modo = Modos.normal;
                return rules.currentMusic.acompanhamento;
            case 2:
                rules.modo = Modos.lento;
                return rules.currentMusic.audioClipLento;
            case 3:
                rules.modo = Modos.lento;
                return rules.currentMusic.solo; // lenta sem acompanhamento.
        }
        return audioSource.clip = rules.currentMusic.audioClip;
    }
}
