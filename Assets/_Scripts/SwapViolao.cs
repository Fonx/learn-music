﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SwapViolao : MonoBehaviour
{
    public RollingViolao rules;
    public TMP_Dropdown dropDown;
    public AudioSource audioSource;

    public void Start()
    {

    }
    public void ChangeGameMode()
    {
        audioSource.Stop();
        rules.PlayMusic();
    }
    public AudioClip ReturnAudioClip()
    {
        switch (dropDown.value)
        {
            case 0:
                rules.modo = Modos.normal;
                return rules.currentMusic.audioClip;
            case 1:
                rules.modo = Modos.lento;
                return rules.currentMusic.audioClipLento;
        }
        return audioSource.clip = rules.currentMusic.audioClip;
    }
}
