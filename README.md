# Learn-Music

This application was built with the aim of teaching music (flute and guitar) in a fun and interactive way. Users can learn how to play chords and songs through various practices.

## Practice1:

Teach two scales of the notes.
Indicate if the sound is performed correctly with the virtual tuning fork.
Show the correct sound of notes.

## Practice2:

This practice works like Guitar Hero, where the user must play the flute along with the music. A particle system emits green particles if the user plays correctly, and red particles if the user makes mistakes.

## Practice3:

This is the most advanced practice, and it works like a band. The student will listen to a song that contains more than one instrument and must play flute 1 and listen to flute 2 or listen to flute 2 and play flute 1.

## How it works?

The tuning fork code was written in JavaScript by Chris Wilson and is licensed under the MIT License (MIT) copyright (c) 2014.

This tuning fork captures the frequency used by the user's microphone and indicates the related frequency band. Using a JavaScript/C# bridge, we can code a conversion of frequency bands to musical notes. In this way, we can determine if the user is playing the correct note at a specific time and provide feedback through the particle system.

## Guitar practice

The guitar project was discontinued, but it is still possible to practice learning guitar with the application.

## Usage

Go to _scenes -> Build -> merge with the template.

# Examples:

Second practice:
*Click "Enable microphone" in the upper left corner after the scene has loaded.
https://learn-music-build.vercel.app/


# Learn-Music

Esta aplicação foi construída com o objetivo de ensinar música (flauta doce e violão) de forma lúdica. O usuário aprende na prática como fazer os principais acordes e como tocar algumas músicas.


## Prática1 :

Ensinar duas escalas das notas dó,ré,mi,fá,sol,lá,si.
Indicar se o som capturado está na escala correta com o diapasão virtual.
Mostrar o som correto das notas.

## Prática2

Esta prática funciona como um guitar hero, o usuário deve tocar a flauta ao mesmo tempo em que a música toca.
Conforme o usuário toca a música de forma aceitável um particle system verde é emitido e caso contrário um vermelho.

## Prática3

Esta é a prática mais avançada, funciona como uma banda. O aluno vai ouvir uma música que contém mais de um instrumento, deve ouvir a flauta 1 e tocar a flauta 2 ou ouvir a flauta 2 e tocar a flauta 1.

## Como funciona?

O código do diapasão foi feito em Js por Chris Wilson e possúi licença do: The MIT License (MIT) Copyright (c) 2014.
Esse diapasão captura a frequencia emitida pelo microfone do usuário e indica a banda de frequencia relacionada, esse foi o maior incentivo para o desenvolvimento do projeto.

Utilizando uma ponte js/c#, dentro do código da flauta é realizado a conversão de bandas de frequencia para uma Nota musical.
Dessa forma temos a nota que o usuário está tocando e podemos comparar com a nota esperada, indicando com o particle system da cor verde caso o usuário esteja tocando da forma correta e vermelha caso a nota esteja longe da correta.


## Práticas com violão

 O projeto do violão foi descontinuado mas é possível realizar práticas para aprender violão assim como a flauta.




#Exemplos:

Segunda Prática:
*clique em habilitar microfone no canto superior esquerdo apôs o carregamento da cena
https://learn-music-build.vercel.app/
